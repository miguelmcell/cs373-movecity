import React, { Component } from 'react'
import * as d3 from "d3"
import './Slice.css'

class Slice extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        let arc = d3.arc()
          .innerRadius(this.props.innerRadius)
          .outerRadius(this.props.outerRadius)
          .startAngle(this.props.startAngle)
          .endAngle(this.props.endAngle)
    
        return (
          <g>
            <path d={arc()} fill={this.props.fillColor} />
          </g>
        )
      }
    
}

export default Slice