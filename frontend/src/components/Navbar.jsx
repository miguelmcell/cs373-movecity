import React, {Component} from 'react'

export default class Navbar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // search values
			searchValues: '',
        }

        this.handleSearchChange = this.handleSearchChange.bind(this)
        this.handleSearchSubmit = this.handleSearchSubmit.bind(this)
    }

    handleSearchChange(event) {
		this.setState({
			searchValues: event.target.value
		})
	}

    handleSearchSubmit() {
        console.log(this.props)
        this.props.history.push({pathname: '/search', state: {searchValues: this.state.searchValues}})
    }

	render() {
		return (
			<div>
				<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <a className="navbar-brand" href="/">MoveCity</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarText">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <a className="nav-link" href="/">Home</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/quality-of-life">Quality Of Life</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/climate">Climate</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/crime">Crime</a>
                            </li>
                            <li className="nav-item dropdown">
                                <div className="btn-group">
                                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Visualizations
                                    </a>
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a className="dropdown-item" href="/movecity-visualization">MoveCity</a>
                                        <div className="dropdown-divider"></div>
                                        <a className="dropdown-item" href="/dev-visualization">Endangered Animals</a>
                                    </div>
                                </div>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/about">About</a>
                            </li>
                        </ul>
                        <div className="form-inline">
                            <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" onChange={this.handleSearchChange} />
                            <button className="btn btn-primary btn-rounded my-2 my-sm-0" onClick={this.handleSearchSubmit} >Search</button>
                        </div>
                    </div>
                </nav>
			</div>
		);
	}
}
