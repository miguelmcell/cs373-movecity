import React, {Component} from 'react'

export default class Footer extends Component {
	render() {
		return (
			<div>
                <footer className="footer mt-auto py-3 bg-dark">
                    <div className="container">
                        <span className="text-muted text-center">
                            <p>&copy; MoveCity | Privacy Policy | Terms of Service</p>
                        </span>
                    </div>
                </footer>
            </div>
		)
	}
}