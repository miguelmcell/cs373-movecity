import React from 'react';
import {Router, Route} from 'react-router';

import App from './App';
import About from './pages/About';
import City from './pages/City';
import State from './pages/State';
import QualityOfLife from './pages/QualityOfLife';

const Routes = (props) => (
    <Router {...props}>
        <Route path="/" component={App} />
        <Route path="/about" component={About} />
        <Route path="/city" component={City} />
        <Route path="/state" component={State} />
        <Route path="/quality-of-life" component={QualityOfLife} />
    </Router>
);
