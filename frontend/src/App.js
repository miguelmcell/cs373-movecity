import React, {Component} from 'react';
import {BrowserRouter as Router, Switch, Route, withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import logo from './logo.svg';
import './App.css';
import LandingPage from './pages/LandingPage';
import About from './pages/About';
import City from './pages/City';
import QualityOfLife from './pages/QualityOfLife';
import QualityOfLifeLosAngeles from './pages/QualityOfLifeLosAngeles';
import QualityOfLifeChicago from './pages/QualityOfLifeChicago';
import QualityOfLifeNewYork from './pages/QualityOfLifeNewYork';
import State from './pages/State';
import Climate from './pages/Climate';
import ClimateLosAngeles from './pages/ClimateLosAngeles';
import ClimateChicago from './pages/ClimateChicago';
import ClimateNewYork from './pages/ClimateNewYork';
import PageNotFound from './pages/PageNotFound';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import Crime from './pages/Crime';
import CrimeLosAngeles from './pages/CrimeLosAngeles';
import CrimeChicago from './pages/CrimeChicago';
import CrimeNewYork from './pages/CrimeNewYork';

import ClimateCity from './pages/ClimateCity';
import CrimeCity from './pages/CrimeCity';
import QualityOfLifeCity from './pages/QualityOfLifeCity';

import SearchPage from './pages/SearchPage';
import MoveCityVisualization from './pages/MoveCityVisualization';
import DevVisualization from './pages/DeveloperVisualization';

class App extends Component {
  render() {
    return (
      <div className="App Site">
        <div className="Site-content">
          <div className="Site-header">
            <Navbar history={this.props.history}/>
          </div>
          <div>
            {/* <Router> */}
              <div className="main">
                <Switch>
                  <Route exact path="/" component={LandingPage} />
                  <Route path="/about" component={About} />
                  <Route path="/quality-of-life" component={QualityOfLife} />
                  <Route path="/quality-of-life-new-york" component={QualityOfLifeNewYork} />
                  <Route path="/quality-of-life-los-angeles" component={QualityOfLifeLosAngeles} />
                  <Route path="/quality-of-life-chicago" component={QualityOfLifeChicago} />
                  <Route path="/climate" component={Climate} />
                  <Route path="/climate-new-york" component={ClimateNewYork} />
                  <Route path="/climate-los-angeles" component={ClimateLosAngeles} />
                  <Route path="/climate-chicago" component={ClimateChicago} />
                  <Route path="/crime-new-york" component={CrimeNewYork} />
                  <Route path="/crime-los-angeles" component={CrimeLosAngeles} />
                  <Route path="/crime-chicago" component={CrimeChicago} />
                  <Route path="/crime" component={Crime} />

                  <Route path="/climate-city" component={ClimateCity} />
                  <Route path="/crime-city" component={CrimeCity} />
                  <Route path="/quality-of-life-city" component={QualityOfLifeCity} />

                  <Route path="/city" component={City} />

                  <Route path="/search" component={SearchPage} />
                  <Route path="/dev-visualization" component={DevVisualization} />

                  {/* <Route path="/dev-visualization" component={DevVisualization} /> */}
                  <Route path="/movecity-visualization" component={MoveCityVisualization} />

                  <Route path="*" component={PageNotFound} />
                </Switch>
              </div>
            {/* </Router> */}
          </div>
        </div>
        <Footer />
      </div>

    );
  }
}

export default withRouter(App);
