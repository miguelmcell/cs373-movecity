import React, {Component} from 'react'
import qolImg from '../img/NYC-QOL.jpg'

export default class QualityOfLifeNewYork extends Component {


	render() {
		return (
			<div className="text-center">
				<br /><br />
				<h1 className="text-center">New York City Quality of Life</h1>
				<br /><br />
                <img src={qolImg} width="100%" height="400px"/>
				<h3>Housing: 1.0</h3>	
				<h3>Cost of Living: 2.3</h3>
				<h3>Commute: 5.5</h3>
				<h3>Healthcare: 8.5</h3>
				<h3>Education: 8.0</h3>
				<h3>Environmental Quality: 5.2</h3>
				<h3>Tax: 3.9</h3>
				<h3>Economy: 6.5</h3>
				<h3>Internet Access: 5.5</h3>
				<h3>Safety: 5.7</h3>
				<h3>New York Climate data: <a href="/climate-new-york">click here</a></h3>
				<h3>New York Crime data: <a href="/crime-new-york">click here</a></h3>
            </div>
			
		)
	}
}
