import React, { Component } from 'react'
import axios from 'axios'
import * as d3 from 'd3'
import uStates from '../components/uState'
import './UsaStateMap.css'

var usaStates = {
    'AL': 'Alabama',
    'AK': 'Alaska',
    'AZ': 'Arizona',
    'AR': 'Arkansas',
    'CA': 'California',
    'CO': 'Colorado',
    'CT': 'Connecticut',
    'DE': 'Delaware',
    'FL': 'Florida',
    'GA': 'Georgia',
    'HI': 'Hawaii',
    'ID': 'Idaho',
    'IL': 'Illinois',
    'IN': 'Indiana',
    'IA': 'Iowa',
    'KS': 'Kansas',
    'KY': 'Kentucky',
    'LA': 'Louisiana',
    'ME': 'Maine',
    'MD': 'Maryland',
    'MA': 'Massachusetts',
    'MI': 'Michigan',
    'MN': 'Minnesota',
    'MS': 'Mississippi',
    'MO': 'Missouri',
    'MT': 'Montana',
    'NE': 'Nebraska',
    'NV': 'Nevada',
    'NH': 'New Hampshire',
    'NJ': 'New Jersey',
    'NM': 'New Mexico',
    'NY': 'New York',
    'NC': 'North Carolina',
    'ND': 'North Dakota',
    'OH': 'Ohio',
    'OK': 'Oklahoma',
    'OR': 'Oregon',
    'PA': 'Oregon',
    'RI': 'Rhode Island',
    'SC': 'South Carolina',
    'SD': 'South Dakota',
    'TN': 'Tennessee',
    'TX': 'Texas',
    'UT': 'Utah',
    'VT': 'Vermont',
    'VA': 'Virginia',
    'WA': 'Washington',
    'WV': 'West Virginia',
    'WI': 'Wisconsin',
    'WY': 'Wyoming',
    'DC': 'District of Columbia'
}

class UsaStateMap extends Component {
    constructor() {
        super()
        this.state = {
            loading: true,
            qolData: [],
        }
    }

    async componentDidMount() {
		try {
			const result = await axios.get('https://movecity.live/v1/api/QOL')
			this.setState({
                qolData: result.data.objects,
                loading: false
			});
		} catch (error) {
			this.setState({
				error,
				loading: false
			});
		}
	}

    getAvg(data) {
        var avg = {}
        data.forEach(d => {
            for(var category in d) {
                if(!isNaN(Number(d[category]))) {
                    if(category in avg) {
                        avg[category] += Number(d[category])
                    } else {
                        avg[category] = Number(d[category])
                    }
                }
            }
        });

        for(var category in avg) {
            avg[category] = 1.0 * avg[category] / data.length
        }

        return avg
    }

    getStateAvg(data) {
        let states = {}
        let stateAvg = {}
        data.forEach(d => {
            let state = d["State"].replace(/^(?:')(.*)(?:')$/, "$1")
            delete d["QOL_idCity"]
            if(state in states) {
                states[state].push(d)
            } else {
                states[state] = [d]
            }
        })

        for(var state in states) {
            stateAvg[state] = this.getAvg(states[state])
        }

        return stateAvg
    }

    renderQolMap(states) {
        var mapData = {}
        for(var state in usaStates) {
            if(states !== undefined && states.length !== 0){
                var d = {}
                if(usaStates[state] in states) {
                    d = JSON.parse(JSON.stringify(states[usaStates[state]]))
                    d['color'] = d3.interpolate("white", "black")(d['Housing'] / 10)
                }
                mapData[state] = d
            }
        }

        function tooltipHtml(name, data) {
            let str = ""
            try{
                str = "<h4>" + name+ "</h4><table>"+
                "<tr><td>" + ("Housing: ") + "</td><td>" + (data.Housing.toFixed(2)) + "</td></tr>" +
                "<tr><td>" + ("Cost of Living: ") + "</td><td>" + (data.Cost_of_Living.toFixed(2)) + "</td></tr>" +
                "<tr><td>" + ("Commute: ") + "</td><td>" + (data.Commute.toFixed(2)) + "</td></tr>" +
                "<tr><td>" + ("Healthcare: ") + "</td><td>" + (data.Healthcare.toFixed(2)) + "</td></tr>" +
                "<tr><td>" + ("Education: ") + "</td><td>" + (data.Education.toFixed(2)) + "</td></tr>" +
                "<tr><td>" + ("Environmental Quality: ") + "</td><td>" + (data.Environmental_Quality.toFixed(2)) + "</td></tr>" +
                "<tr><td>" + ("Tax: ") + "</td><td>" + (data.Tax.toFixed(2)) + "</td></tr>" +
                "</table>";
            } catch {
                str = "<h4></h4>"
            }

            return str
        }

        uStates.draw("#statesvg", mapData, tooltipHtml);
        d3.select(window.frameElement).style("height", "100%")
    }

    render() {
        if(!this.state.loading) {
            const state = this.getStateAvg(this.state.qolData)
            this.renderQolMap(state)
        }

        return (
            <div>
                <div id="tooltip"></div>
                <svg width="100%" height="600" id="statesvg"></svg>
            </div>
        )
    }
}

export default UsaStateMap
