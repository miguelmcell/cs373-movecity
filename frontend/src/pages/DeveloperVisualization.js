import React, { Component } from 'react';
import * as d3 from "d3";
import USMap from './USMap';

import './DeveloperVisualization.css';

import WorldMap from "./WorldMap";
import ProtectedAreas from "./ProtectedAreas";
import TotalAreaPerCountry from "./TotalArea";

export default class DeveloperVisualization extends Component {
	constructor(props) {
    super(props);
    this.state = {
      hasMounted : 0
    };
  }
	render() {
		return (
			<div style={{paddingLeft: '200px'}}>
			 <h2 style={{paddingLeft: '300px', paddingTop: '30px'}} >
			 Endagered Animals Around the World
			 </h2>
			 <br/>
			 <div>
			 	<WorldMap/>
			 </div>
			 <h2 style={{paddingLeft: '300px'}} >
			 Protected Areas Around the World
			 </h2>
			 <br/>
			 <ProtectedAreas />
			 <br/>
			 <h2 style={{paddingLeft: '350px'}} >
			 Total Number of Protected Areas(sq. mi) per Country
			 </h2>
			 <br/>
			 <TotalAreaPerCountry/>
			 <br/>
			</div>
		 )
  }

}
