import React, { Component } from 'react'
import UsaStateMap from './UsaStateMap'
import PieChart from './PieChart'
import BarChart from './BarChart'
          
class MoveCityVisualization extends Component {
    constructor() {
        super()
        this.state = {
        }
    }

    render() {
        return (
            <div className="container">
                <div className="col mt-5 mb-5">
                    <h1 className="text-center">Quality Of Life Averages Per State Map</h1>
                    <UsaStateMap />
                </div>
                <div className="col mb-5">
                    <h1 className="text-center">Crime Averages Per State Pie Chart</h1>
                    <PieChart />
                </div>
                <div className="col mb-5">
                    <h1 className="text-center">Average Temperature Per State Bar Chart</h1>
                    <BarChart />
                </div>
            </div>
        )
    }
}

export default MoveCityVisualization