import React, {Component} from 'react'
import axios from 'axios'
import './QualityOfLife.css'
import Table from '../components/Table'
import ReactPaginate from 'react-paginate'

// Table header
const tableHeader = ["City", "Housing", "Cost of Living", "Commute", "Healthcare", "Education", "Environmental Quality", "Tax"]

class QualityOfLife extends Component {
	constructor() {
        super()
        this.state = {
			tableRows: [],
			loading: true,
            currentPage: 1,
            tableRowsPerPage: 10,
			sortDirection: '',
			sortColumn: '',

			// filter values
			filter: '',
			housingValue: '',
			housingOp: '',
			costOfLivingValue: '',
			costOfLivingOp: '',
			commuteValue: '',
			commuteOp: '',
			healthcareValue: '',
			healthcareOp: '',
			educationValue: '',
			educationOp: '',
			environmentalQualityValue: '',
			environmentalQualityOp: '',
			taxValue: '',
			taxOp: '',

			// search values
			searchValues: '',
			search: '',

		}
		this.sortData = this.sortData.bind(this)
		this.handleFilterChange = this.handleFilterChange.bind(this)
		this.handleFilterSubmit = this.handleFilterSubmit.bind(this)
		this.handleFilterReset = this.handleFilterReset.bind(this)
		this.handleSearchSubmit = this.handleSearchSubmit.bind(this)
		this.handleSearchChange = this.handleSearchChange.bind(this)
    }
    
    async componentDidMount() {
		this.loadData()
	}

	async loadData() {
		try {
			let filters = ''
			if(this.state.filter !== '') {
				filters = `?q={"filters":${this.state.filter}}`
			} 
			// else if(this.state.search !== '') {
			// 	filters = `?q={"filters":${this.state.search}}`
			// }
			const result = await axios.get(`/v1/api/QOL${filters}`)
			this.setState({
				tableRows: result.data.objects,
				loading: false
			});
		} catch (error) {
			this.setState({
				error,
				loading: false
			});
		}
	}

    changePage = data  => {
        this.setState({
            currentPage: data.selected + 1
        })
    }

    sortData(sortDirection, sortColumn) {
		const columns = {"City": "QOL_idCity", "Housing": "Housing", "Cost of Living": "Cost_of_Living", "Commute": "Commute", "Healthcare": "Healthcare", "Education": "Education", "Environmental Quality": "Environmental_Quality", "Tax": "Tax"}
		let sortedData = []
		if(sortDirection === 'asc') {
			sortedData = this.state.tableRows.sort(function(a, b){return a[columns[sortColumn]]-b[columns[sortColumn]]})
		} else {
			sortedData = this.state.tableRows.sort(function(a, b){return b[columns[sortColumn]]-a[columns[sortColumn]]})
		}

		this.setState({
			tableRows: sortedData,
			currentPage: 1
		})
	}

	checkNaNFilters(...args) {
		args.forEach(arg => {
			if(isNaN(Number(arg))) {
				alert(`"${arg}" is not a number`)
				return false
			}
		})
		return true
	}

	handleFilterSubmit(event) {
		if(this.checkNaNFilters(this.state.housingValue, this.state.costOfLivingValue, this.state.commuteValue, this.state.healthcareValue, this.state.educationValue, this.state.environmentalQualityValue, this.state.taxValue)) {
			let filter = [{"and": []}]
			if(this.state.housingOp !== '' && this.state.housingValue !== '') {
				console.log("this.state.housingOp:" + this.state.housingOp)
				const op = this.state.housingOp === "Greater Than" ? "gt" : this.state.housingOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Housing", "op": op, "val": Number(this.state.housingValue)})
			}
			if(this.state.costOfLivingOp !== '' && this.state.costOfLivingValue !== '') {
				const op = this.state.costOfLivingOp === "Greater Than" ? "gt" : this.state.costOfLivingOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Cost_of_Living", "op": op, "val": Number(this.state.costOfLivingValue)})
			}
			if(this.state.commuteOp !== '' && this.state.commuteValue !== '') {
				const op = this.state.commuteOp === "Greater Than" ? "gt" : this.state.commuteOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Commute", "op": op, "val": Number(this.state.commuteValue)})
			}
			if(this.state.healthcareOp !== '' && this.state.healthcareValue !== '') {
				const op = this.state.healthcareOp === "Greater Than" ? "gt" : this.state.healthcareOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Healthcare", "op": op, "val": Number(this.state.healthcareValue)})
			}
			if(this.state.educationOp !== '' && this.state.educationValue !== '') {
				const op = this.state.educationOp === "Greater Than" ? "gt" : this.state.educationOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Education", "op": op, "val": Number(this.state.educationValue)})
			}
			if(this.state.environmentalQualityOp !== '' && this.state.environmentalQualityValue !== '') {
				const op = this.state.environmentalQualityOp === "Greater Than" ? "gt" : this.state.environmentalQualityOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Environmental_Quality", "op": op, "val": Number(this.state.environmentalQualityValue)})
			}
			if(this.state.taxOp !== '' && this.state.taxValue !== '') {
				const op = this.state.taxOp === "Greater Than" ? "gt" : this.state.taxOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Tax", "op": op, "val": Number(this.state.taxValue)})
			}

			this.setState({
				filter: JSON.stringify(filter),
				loading: true
			},() => this.loadData())
		}

		event.preventDefault()
	}

	handleFilterReset(event) {
		this.setState({
			filter: '',
			housingValue: '',
			housingOp: '',
			costOfLivingValue: '',
			costOfLivingOp: '',
			commuteValue: '',
			commuteOp: '',
			healthcareValue: '',
			healthcareOp: '',
			educationValue: '',
			educationOp: '',
			environmentalQualityValue: '',
			environmentalQualityOp: '',
			taxValue: '',
			taxOp: '',
			search: '',
			searchValues: '',
		}, this.loadData)
		alert("You have reset the table")
	}

	handleFilterChange(event) {
		const target = event.target;
		const name = target.name;
		const value = target.value

		this.setState({
			[name]: value
		});
	}

	handleSearchChange(event) {
		this.setState({
			searchValues: event.target.value
		})
	}

	handleSearchSubmit() {
		// const columns = ["Housing", "Cost_of_Living", "Commute", "Healthcare", "Education", "Environmental_Quality", "Tax"]
		// let filter = [{"or": []}]
		// const words = this.state.searchValues.split(" ")
		// words.forEach(word => {
		// 	columns.forEach(comumn => {
		// 		filter[0].or.push({"name": comumn, "op": "eq", "val": word})
		// 	})

		// 	// DOESNT WORK BECAUSE ERROR 431 (Request Header Fields Too Large) :(
		// 	this.state.cityData.forEach(city => {
		// 		if(city.CityName.toLowerCase().includes(word)) {
		// 			filter[0].or.push({"name": "QOL_idCity", "op": "eq", "val": city.idCity})
		// 		}
		// 	})
		// })

		// this.setState({
		// 	search: JSON.stringify(filter)
		// }, this.loadData)

		const words = this.state.searchValues.split(" ")
		const columns = ["QOL_idCity", "Housing", "Cost_of_Living", "Commute", "Healthcare", "Education", "Environmental_Quality", "Tax"]
		const searchData = this.state.tableRows.filter(row => {
			for(let i = 0; i < words.length; i++) {
				for(let j = 0; j < columns.length; j++) {
					if(columns[j] === "QOL_idCity" && isNaN(Number(words[i]))) {
						if(row["CityName"].toLowerCase().includes(words[i].toLowerCase())) {
							return true
						} else {
							break
						}
					} else if(!isNaN(Number(words[i]))){
						if(row[`${columns[j]}`] === Number(words[i])) {
							return true
						}
					}
				}
			}
			return false
		})

		this.setState({
			tableRows: searchData
		})

	}
    
    render() {

		if(this.state.loading) {
            return <h2>Loading...</h2>
        }
        // Get current rows
        const indexOfLastRow = this.state.currentPage * this.state.tableRowsPerPage
        const indexOfFirstRow = indexOfLastRow - this.state.tableRowsPerPage
        const currentRows = this.state.tableRows.slice(indexOfFirstRow, indexOfLastRow)
		const totalPages = Math.ceil(this.state.tableRows.length / this.state.tableRowsPerPage)
		
		return (
			
			<div className="qol">
				<br /><br />
				<h1 className="text-center">Quality Of Life</h1>
				<br /><br />
				<div className="qol-content">
					<div className="filter mb-3">
						<div className="form-group row mx-1">
							<button className="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
								Filter Search Results
							</button>
							<div className="col-xs-4 ml-5 mr-1">
								<input className="form-control" type="text" placeholder="Search" aria-label="Search" onChange={this.handleSearchChange}/>
								{/* <button className="btn aqua-gradient btn-rounded btn-sm my-0" type="submit">Search</button> */}
							</div>
							<div className="col-xs-4 mr-1">
								{/* <input className="form-control" type="text" placeholder="Search" aria-label="Search" /> */}
								<button className="btn btn-primary btn-rounded btn" type="submit" onClick={this.handleSearchSubmit}>Search</button>
							</div>
							<div className="col-xs-4">
								<button className="btn btn-primary btn-rounded btn" type="reset" onClick={this.handleFilterReset}>Reset</button>
							</div>
						</div>
						<div className="collapse" id="collapseFilter">
							<div className="card card-body bg-dark">
								<form className="container" onSubmit={this.handleFilterSubmit} onReset={this.handleFilterReset} >
									<label className="row">
										Housing:
										<select className="mx-2" name="housingOp" value={this.state.housingOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="housingValue" value={this.state.housingValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Cost Of Living:
										<select className="mx-2" name="costOfLivingOp" value={this.state.costOfLivingOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="costOfLivingValue" value={this.state.costOfLivingValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Commute:
										<select className="mx-2" name="commuteOp" value={this.state.commuteOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="commuteValue" value={this.state.commuteValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Healthcare:
										<select className="mx-2" name="healthcareOp" value={this.state.healthcareOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="healthcareValue" value={this.state.healthcareValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Education:
										<select className="mx-2" name="educationOp" value={this.state.educationOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="educationValue" value={this.state.educationValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Environmental Quality:
										<select className="mx-2" name="environmentalQualityOp" value={this.state.environmentalQualityOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="environmentalQualityValue" value={this.state.environmentalQualityValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Tax:
										<select className="mx-2" name="taxOp" value={this.state.taxOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="taxValue" value={this.state.taxValue} onChange={this.handleFilterChange} />
									</label>

									<input className="mx-2 my-2" type="submit" value="Submit" />
									<input type="reset" value="Reset" />
								</form>
							</div>
						</div>
					</div>
					<Table tableRows={currentRows} loading={this.state.loading} tableHeader={tableHeader} history={this.props.history} path={'/quality-of-life-city'} modelType={'quality-of-life'} doSort={true} sortData={this.sortData.bind(this)} searchWords={this.state.searchValues.split(" ")} />
					<div className="page-num">
						<ReactPaginate
							previousLabel={'Previous'}
							nextLabel={'Next'}
							breakLabel={'...'}
							pageCount={totalPages}
							marginPagesDisplayed={2}
							pageRangeDisplayed={3}
							onPageChange={this.changePage}
							forcePage={this.state.currentPage-1}
							breakClassName={'page-item'}
							breakLinkClassName={'page-link'}
							containerClassName={'pagination'}
							pageClassName={'page-item'}
							pageLinkClassName={'page-link'}
							previousClassName={'page-item'}
							previousLinkClassName={'page-link'}
							nextClassName={'page-item'}
							nextLinkClassName={'page-link'}
							activeClassName={'active'}
						/>

					</div>
				</div>
			</div>
		)
	}
}

export default QualityOfLife
