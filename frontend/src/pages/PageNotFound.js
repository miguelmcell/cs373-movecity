import React, {Component} from 'react'

export default class PageNotFound extends Component {
	render() {
		return (
			<div>
				<div>
					The page you are looking for does not exist. Please go back to the home page 
                    or navigate to another page from the navigation bar.
				</div>
			</div>
		)
	}
}