import React, {Component} from 'react'
import crimeImg from '../img/crime-pic.jpg'

export default class CrimeLosAngeles extends Component {

	render() {
		return (
			<div className="text-center">
				<br /><br />
				<h1 className="text-center">Los Angeles Crime</h1>
				<br /><br />
                <img src={crimeImg} width="100%" height="400px"/>
				<h3>Aggravated Assault: 3559</h3>	
				<h3>Arson: 215</h3>
				<h3>Burglary: 3739</h3>
				<h3>Homicide: 98</h3>
				<h3>Human Trafficing: 0</h3>
				<h3>Larceny: 8359</h3>
				<h3>Property Crime: 4203</h3>
				<h3>Vehicle Theft: 16301</h3>
				<h3>Rape: 304</h3>
				<h3>Robbery: 1212</h3>
				<h3>Violent Crime: 5173</h3>
				<h3>Los Angeles Climate data: <a href="/climate-los-angeles">click here</a></h3>
				<h3>Los Angeles Quality of Life data: <a href="/quality-of-life-los-angeles">click here</a></h3>
            </div>
		)
	}
}
