import React, { Component } from 'react'
import axios from 'axios'
import * as d3 from 'd3'
import Slice from '../components/Slice'

const categories = ["Aggravated_Assault", "Arson", "Burglary", "Homicide", "Larceny", "Property_Crime", "Vehicle_Theft", "Robbery", "Violent_Crime"]
const categoriesShort = ["Aggravated Assault", "Arson", "Burglary", "Homicide", "Larceny", "Property Crime", "Vehicle Theft", "Robbery", "Violent Crime"]
const stateNames = Object.values({
    'AL': 'Alabama',
    'AK': 'Alaska',
    'AZ': 'Arizona',
    'AR': 'Arkansas',
    'CA': 'California',
    'CO': 'Colorado',
    'CT': 'Connecticut',
    'DE': 'Delaware',
    'GA': 'Georgia',
    'HI': 'Hawaii',
    'ID': 'Idaho',
    'IL': 'Illinois',
    'IN': 'Indiana',
    'IA': 'Iowa',
    'KS': 'Kansas',
    'KY': 'Kentucky',
    'LA': 'Louisiana',
    'ME': 'Maine',
    'MD': 'Maryland',
    'MI': 'Michigan',
    'MN': 'Minnesota',
    'MS': 'Mississippi',
    'MO': 'Missouri',
    'MT': 'Montana',
    'NE': 'Nebraska',
    'NV': 'Nevada',
    'NH': 'New Hampshire',
    'NJ': 'New Jersey',
    'NM': 'New Mexico',
    'NY': 'New York',
    'NC': 'North Carolina',
    'ND': 'North Dakota',
    'OH': 'Ohio',
    'OK': 'Oklahoma',
    'OR': 'Oregon',
    'PA': 'Pennsylvania',
    'RI': 'Rhode Island',
    'SD': 'South Dakota',
    'TN': 'Tennessee',
    'TX': 'Texas',
    'UT': 'Utah',
    'VT': 'Vermont',
    'VA': 'Virginia',
    'WA': 'Washington',
    'WV': 'West Virginia',
    'WI': 'Wisconsin',
    'WY': 'Wyoming',
}).sort()

class PieChart extends Component {
    constructor() {
        super()
        this.state = {
            loading: true,
            crimeStateAvgData: [],
            currState: 'Texas',

        }
        this.colorScale = d3.scaleOrdinal(d3.schemeCategory10)
        this.renderDropdown = this.renderDropdown.bind(this)
    }

    async componentDidMount() {
		try {
            let result = await axios.get('https://movecity.live/v1/api/Crime')
            console.log(result)
            let stateAvg = this.getStateAvg(result.data.objects)
            console.log(stateAvg)
			this.setState({
                crimeStateAvgData: stateAvg,
                loading: false
			});
		} catch (error) {
			this.setState({
				error,
				loading: false
			});
		}
    }

    getAvg(data) {
        var avg = {}
        data.forEach(d => {
            for(var category in d) {
                if(!isNaN(Number(d[category]))) {
                    if(category in avg) {
                        avg[category] += Number(d[category])
                    } else {
                        avg[category] = Number(d[category])
                    }
                }
            }
        });

        for(var category in avg) {
            avg[category] = 1.0 * avg[category] / data.length
        }

        return avg
    }

    getStateAvg(data) {
        let states = {}
        let stateAvg = {}
        data.forEach(d => {
            let state = d["State"].replace(/^(?:')(.*)(?:')$/, "$1")

            delete d["Crime_idCity"]
            if(state in states) {
                states[state].push(d)
            } else {
                states[state] = [d]
            }
        })

        for(var state in states) {
            stateAvg[state] = this.getAvg(states[state])
        }

        return stateAvg
    }

    renderDropdown() {
        if (!this.state.loading) {
            console.log("stateNames", stateNames)
            return (
                <div className="dropdown text-center my-3">
                    <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {"State: " + this.state.currState}
                    </button>
                    <div className="dropdown-menu" style={{ maxHeight: "60vh", overflowY: "auto" }} aria-labelledby="dropdownMenuButton">
                        {
                            stateNames.map((name, index) => {
                                return <a key={index} className="dropdown-item" onClick={() => this.setState({ currState: name })}>{name}</a>
                            })
                        }
                        {/* <a key={0} className="dropdown-item" onClick={() => this.setState({ currState: 'Texas' })}>Texas</a>
                        <a key={1} className="dropdown-item" onClick={() => this.setState({ currState: 'Alabama' })}>Alabama</a>
                        <a key={2} className="dropdown-item" onClick={() => this.setState({ currState: 'Washington' })}>Washington</a> */}
                    </div>
                </div>
            )
        }

        return <div></div>;
    }

    componentDidUpdate() {
        if (!this.state.loading) {
            const svg = d3.select("#pie")
            const dimensions = svg.node().getBoundingClientRect()
            svg.attr("viewBox", `${-dimensions.width / 2} ${-dimensions.height / 2} ${dimensions.width} ${dimensions.height}`)
        }
    }

    render() {
        if(this.state.loading) {
            return <div>Loading...</div>
        }

        let crimeData = []
        // console.log("this.state.crimeStateAvgData", this.state.crimeStateAvgData)
        categories.forEach(c => {
            crimeData.push(this.state.crimeStateAvgData[this.state.currState][c])
        })
        let crimePie = d3.pie()(crimeData)

        return (
            <div className="box">
                {this.renderDropdown()}
                <svg width="calc(100% - 20)" height="500" id="pie" style={{"paddingTop": '10px'}}>
                    {crimePie.map((obj, i) =>
                        <Slice
                            key={"slice" + i}
                            innerRadius = {0}
                            outerRadius = {250}
                            startAngle = {obj.startAngle}
                            endAngle = {obj.endAngle}
                            fillColor = {this.colorScale(i)}
                        />
                    )}
                    {crimePie.map((obj, i) =>
                        <text key={"label" + i}
                            className="slice-text"
                            transform={`translate(${d3.arc()
                                .centroid({
                                    innerRadius: 125,
                                    outerRadius: 250,
                                    startAngle: obj.startAngle,
                                    endAngle: obj.endAngle})})`
                            }
                            textAnchor="middle"
                            fill="white"
                            style={{fontSize: 13}}>

                            <tspan style={{fontSize: 10}}>
                                {categoriesShort[i] + ": " + obj.data.toFixed(2) + " cases"}
                            </tspan>
                        </text>
                    )}
                </svg>


            </div>
        )
    }



}

export default PieChart
