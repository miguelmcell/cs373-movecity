// Created in help from a tutorial by Kacper Goliński for building
// responsive chart with react and d3 v4

import React, { Component } from 'react'
import { scaleBand, scaleLinear } from 'd3-scale'
import axios from 'axios'
import { interpolateLab } from 'd3-interpolate'
import Axes from './Axes'
import ResponsiveWrapper from './ChartComponent'

  class Bars extends Component {
    constructor(props) {
      super(props)

      this.colorScale = scaleLinear()
        .domain([0, this.props.maxValue])
        .range(['#d4af37', '#d4af37'])
        .interpolate(interpolateLab)
    }

    render() {
      const { scales, margins, data, svgDimensions } = this.props
      const { xScale, yScale } = scales
      const { height } = svgDimensions

      const bars = (
        data.map(datum =>
          <rect
            key={datum.state}
            x={xScale(datum.state)}
            y={yScale(datum.Avg_Temp)}
            height={height - margins.bottom - scales.yScale(datum.Avg_Temp)}
            width={xScale.bandwidth()}
            fill={this.colorScale(datum.Avg_Temp)}
          />,
        )
      )

      return (
        <g>{bars}</g>
      )
    }
  }

  class BarChart extends Component {
    constructor() {
      super()
      this.state = {
        loading: true,
        avgTempData: [],
      }
      this.xScale = scaleBand()
      this.yScale = scaleLinear()
    }

    async componentDidMount() {
        try {
            let result = await axios.get('https://movecity.live/v1/api/Climate')
            let stateAvg = this.getStateAvg(result.data.objects, this.state.cityData)

            this.setState({
                avgTempData: stateAvg,
                loading: false
            });
        } catch (error) {
            this.setState({
                error,
                loading: false
            });
        }
    }

    getStateAvg(data) {
        let states = {}
        let stateAvg = []
        data.forEach(d => {
            let state = d["State"].replace(/^(?:')(.*)(?:')$/, "$1")
            if(state in states) {
                states[state].push(d['Avg_Temp'])
            } else {
                states[state] = [d['Avg_Temp']]
            }
        })

        for(var state in states) {
            stateAvg.push({'state': state, 'Avg_Temp': (states[state].reduce((a, b) => a += b) / states[state].length).toFixed(2)})
        }

        return stateAvg
    }

    render() {
      const margins = { top: 50, right: 20, bottom: 100, left: 60 }
      const svgDimensions = {
        width: Math.max(this.props.parentWidth, 300),
        height: 500
      }

      const data = this.state.avgTempData

      const maxValue = 20

      const xScale = this.xScale
        .padding(0.5)
        .domain(data.map(d => d.state))
        .range([margins.left, svgDimensions.width - margins.right])

      const yScale = this.yScale
        .domain([0, maxValue])
        .range([svgDimensions.height - margins.bottom, margins.top])

      return (
        <svg width={svgDimensions.width} height={svgDimensions.height}>
        <Axes
          scales={{ xScale, yScale }}
          margins={margins}
          svgDimensions={svgDimensions}
        />
        <Bars
          scales={{ xScale, yScale }}
          margins={margins}
          data={data}
          maxValue={maxValue}
          svgDimensions={svgDimensions}
        />
      </svg>
      )
    }
  }

  export default ResponsiveWrapper(BarChart)
