import React, { Component } from 'react'
import Geocode from 'react-geocode'
import { Map, GoogleApiWrapper } from 'google-maps-react';
import axios from 'axios'
import './QualityOfLifeCity.css'

// const mapStyles = {
//     height: '50%',
//     width: '50%',
//     position: 'relative'
//   };

class QualityOfLifeCity extends Component {
    constructor(props) {
        super(props)
        this.state = {
			// latitude: 0,
			// longitude: 0,
            isLoading: true,
            imageURL: ''
        }
        this.onClickClimate = this.onClickClimate.bind(this)
        this.onClickCrime = this.onClickCrime.bind(this)
    }
    
    async componentDidMount() {
		try {
			const path = "v1/getCityImage/" + String(this.props.location.state.city) + ",%20" + String(this.props.location.state.state)
			const result = await axios.get(path)
			this.setState({
				imageURL: result.data,
				isLoading: false
			});
		} catch (error) {
			this.setState({
				error,
                isLoading: false,
			});
		}
    }
    
    async onClickClimate(){
        try {
			const result = await axios.get('/v1/api/Climate')
            for(var i = 0; i < result.data.objects.length; i++) {
                if(result.data.objects[i].Climate_idCity === this.props.location.state.id) {
                    this.props.history.push({pathname: '/climate-city', state: { data: result.data.objects[i], city: this.props.location.state.city, state: this.props.location.state.state, id: this.props.location.state.id }})
                    break
                }
            }
			
		} catch (error) {
			this.setState({
				error,
                isLoading: false,
			});
		}
    }
    
    async onClickCrime(){
		try {
			const result = await axios.get('/v1/api/Crime')
            for(var i = 0; i < result.data.objects.length; i++) {
                if(result.data.objects[i].Crime_idCity === this.props.location.state.id) {
                    this.props.history.push({pathname: '/crime-city', state: { data: result.data.objects[i], city: this.props.location.state.city, state: this.props.location.state.state, id: this.props.location.state.id }})
                    break
                }
            }
			
		} catch (error) {
			this.setState({
				error,
                isLoading: false,
			});
		}
	}

    render() {
        // checking if there is no reference for what city they want to see
        try {
            const data = this.props.location.state.data
        } catch(error) {
            return (
                <div>
                    There is no data to be displayed. Please use the homepage or navbar to redirect to
                    this page.
                </div>
            )
        }

        if(this.state.isLoading) {
            return <div>Loading image...</div>
        }

        return(
            <div className="container">
                <div className="row">
                    {/* <Map google={this.props.google} zoom={10} style={mapStyles}
                        initialCenter={{lat: this.state.latitude, lng: this.state.longitude}} /> */}
                    <img src={this.state.imageURL} width="100%" height="400px"/>
                </div>
                <div className="row mt-5">
                    {/* <p>{JSON.stringify(this.props.location.state.data)}</p> */}
                    <table className="table table-striped table-dark">
                        <thead>
                            <tr>
                                <th scope="col">City</th>
                                <th scope="col">Housing</th>
                                <th scope="col">Cost of Living</th>
                                <th scope="col">Commute</th>
                                <th scope="col">Healthcare</th>
                                <th scope="col">Education</th>
                                <th scope="col">Environmental Quality</th>
                                <th scope="col">Tax</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">{this.props.location.state.city}</td>
                                <td>{this.props.location.state.data.Housing}/10</td>
                                <td>{this.props.location.state.data.Cost_of_Living}/10</td>
                                <td>{this.props.location.state.data.Commute}/10</td>
                                <td>{this.props.location.state.data.Healthcare}/10</td>
                                <td>{this.props.location.state.data.Education}/10</td>
                                <td>{this.props.location.state.data.Environmental_Quality}/10</td>
                                <td>{this.props.location.state.data.Tax}/10</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div className="row d-flex justify-content-center">
                    <button type="button" className="btn btn-secondary mx-5 my-5" onClick={this.onClickClimate}>{this.props.location.state.city} Climate Data</button>
                    <button type="button" className="btn btn-secondary mx-5 my-5" onClick={this.onClickCrime}>{this.props.location.state.city} Crime Data</button>
                    <button type="button" className="btn btn-secondary mx-5 my-5">All City Datas</button>
                </div>
            </div>
        )
    }

}

// export default GoogleApiWrapper({
//     apiKey: 'AIzaSyBg4GQQdNSceQbeOmVi5hpbYlAi7FIQvJc'
//   })(QualityOfLifeCity)
export default QualityOfLifeCity