import React, { useState, useEffect } from "react"
import BubbleChart from '@weknow/react-bubble-chart-d3';

let areas =
[
  {
    "country": "Afghanistan",
    "totalArea": 4473.7
  },
  {
    "country": "Albania",
    "totalArea": 890.7319822311395
  },
  {
    "country": "Andorra",
    "totalArea": 68.69999999999999
  },
  {
    "country": "Armenia",
    "totalArea": 7289.56
  },
  {
    "country": "American Samoa",
    "totalArea": 35577.681581
  },
  {
    "country": "Australia",
    "totalArea": 10001.8032000612
  },
  {
    "country": "Austria",
    "totalArea": 674.2782789226621
  },
  {
    "country": "Azerbaijan",
    "totalArea": 2349.436
  },
  {
    "country": "Belgium",
    "totalArea": 71.95420488715175
  },
  {
    "country": "Bangladesh",
    "totalArea": 1615.9427999999998
  },
  {
    "country": "Cyprus",
    "totalArea": 699.9360104203225
  },
  {
    "country": "Czech Republic",
    "totalArea": 3961.15
  },
  {
    "country": "Germany",
    "totalArea": 513.2399915456771
  },
  {
    "country": "Denmark",
    "totalArea": 39.90278655290602
  },
  {
    "country": "Spain",
    "totalArea": 1739.9416724014286
  },
  {
    "country": "Estonia",
    "totalArea": 2194.581009864808
  },
  {
    "country": "Finland",
    "totalArea": 5478.94178962707
  },
  {
    "country": "Fiji",
    "totalArea": 65.0
  },
  {
    "country": "France",
    "totalArea": 3586.0299973487863
  },
  {
    "country": "Faroe Islands",
    "totalArea": 62.87
  },
  {
    "country": "Georgia",
    "totalArea": 675.7400000000001
  },
  {
    "country": "Greece",
    "totalArea": 287.3672979474069
  },
  {
    "country": "Greenland",
    "totalArea": 1962993.5000000002
  },
  {
    "country": "Guam",
    "totalArea": 156.10022362011622
  },
  {
    "country": "Croatia",
    "totalArea": 3163.584907360076
  },
  {
    "country": "Indonesia",
    "totalArea": 17183.6509
  },
  {
    "country": "Sri Lanka",
    "totalArea": 2456.9679
  },
  {
    "country": "Maldives",
    "totalArea": 57.040000000000006
  },
  {
    "country": "Marshall Islands",
    "totalArea": 4660.640947241085
  },
  {
    "country": "Myanmar",
    "totalArea": 1515.1100000000001
  },
  {
    "country": "Mongolia",
    "totalArea": 55850.22000000001
  },
  {
    "country": "Northern Mariana Islands",
    "totalArea": 248535.2367489017
  },
  {
    "country": "Malaysia",
    "totalArea": 6260.089999999999
  },
  {
    "country": "New Caledonia",
    "totalArea": 501.833493
  },
  {
    "country": "Nepal",
    "totalArea": 10272.0
  },
  {
    "country": "New Zealand",
    "totalArea": 24072.05558829955
  },
  {
    "country": "Pakistan",
    "totalArea": 10286.16
  },
  {
    "country": "Philippines",
    "totalArea": 1706.4630000000002
  },
  {
    "country": "Palau",
    "totalArea": 1192.8160463781614
  },
  {
    "country": "Papua New Guinea",
    "totalArea": 6340.6840999999995
  }
]
let labValuePair =
[
  {
    "label": "Afghanistan",
    "value": 44.74
  },
  {
    "label": "Albania",
    "value": 8.91
  },
  {
    "label": "Andorra",
    "value": 0.69
  },
  {
    "label": "Armenia",
    "value": 72.9
  },
  {
    "label": "American Samoa",
    "value": 355.78
  },
  {
    "label": "Australia",
    "value": 100.02
  },
  {
    "label": "Austria",
    "value": 6.74
  },
  {
    "label": "Azerbaijan",
    "value": 23.49
  },
  {
    "label": "Belgium",
    "value": 0.72
  },
  {
    "label": "Bangladesh",
    "value": 16.16
  },
  {
    "label": "Cyprus",
    "value": 7.0
  },
  {
    "label": "Czech Republic",
    "value": 39.61
  },
  {
    "label": "Germany",
    "value": 5.13
  },
  {
    "label": "Denmark",
    "value": 0.4
  },
  {
    "label": "Spain",
    "value": 17.4
  },
  {
    "label": "Estonia",
    "value": 21.95
  },
  {
    "label": "Finland",
    "value": 54.79
  },
  {
    "label": "Fiji",
    "value": 0.65
  },
  {
    "label": "France",
    "value": 35.86
  },
  {
    "label": "Faroe Islands",
    "value": 0.63
  },
  {
    "label": "Georgia",
    "value": 6.76
  },
  {
    "label": "Greece",
    "value": 2.87
  },
  {
    "label": "Greenland",
    "value": 19629.94
  },
  {
    "label": "Guam",
    "value": 1.56
  },
  {
    "label": "Croatia",
    "value": 31.64
  },
  {
    "label": "Indonesia",
    "value": 171.84
  },
  {
    "label": "Sri Lanka",
    "value": 24.57
  },
  {
    "label": "Maldives",
    "value": 0.57
  },
  {
    "label": "Marshall Islands",
    "value": 46.61
  },
  {
    "label": "Myanmar",
    "value": 15.15
  },
  {
    "label": "Mongolia",
    "value": 558.5
  },
  {
    "label": "Northern Mariana Islands",
    "value": 2485.35
  },
  {
    "label": "Malaysia",
    "value": 62.6
  },
  {
    "label": "New Caledonia",
    "value": 5.02
  },
  {
    "label": "Nepal",
    "value": 102.72
  },
  {
    "label": "New Zealand",
    "value": 240.72
  },
  {
    "label": "Pakistan",
    "value": 102.86
  },
  {
    "label": "Philippines",
    "value": 17.06
  },
  {
    "label": "Palau",
    "value": 11.93
  },
  {
    "label": "Papua New Guinea",
    "value": 63.41
  }
]

const TotalArea = () => {

  return (
    <div style={{paddingLeft: '300px'}}>
    <BubbleChart
      graph= {{
        zoom: 1.1,
        offsetX: -0.00,
        offsetY: -0.01,
      }}
      width={1100}
      height={1000}
      padding={5} // optional value, number that set the padding between bubbles
      showLegend={true} // optional value, pass false to disable the legend.
      legendPercentage={30} // number that represent the % of with that legend going to use.
      legendFont={{
            family: 'Arial',
            size: 16,
            color: '#fff',
            weight: 'bold',
          }}
      valueFont={{
            family: 'Arial',
            size: 12,
            color: '#000',
            weight: 'bold',
          }}
      labelFont={{
            family: 'Arial',
            size: 16,
            color: '#000',
            weight: 'bold',
          }}
      data={labValuePair}
      />
    </div>
  )
}

export default TotalArea
