import React, {Component} from 'react'
import axios from 'axios'
import './Climate.css'
import Table from '../components/Table'
import ReactPaginate from 'react-paginate'

// Table header
const tableHeader = ["City", "Precipitation", "Rain Days", "Average Temperature", "Annual High Temperature", "Annual Low Temperature"]

class Climate extends Component {
	constructor() {
        super()
        this.state = {
			tableRows: [],
			loading: true,
			currentPage: 1,
            tableRowsPerPage: 10,
			sortDirection: '',
			sortColumn: '',

			// filter values
			filter: '',
			precipitationValue: '',
			precipitationOp: '',
			rainDaysValue: '',
			rainDaysOp: '',
			averageTempValue: '',
			averageTempOp: '',
			annualHighValue: '',
			annualHighOp: '',
			annualLowValue: '',
			annualLowOp: '',

			// search values
			searchValues: '',
			search: '',
		}
		this.sortData = this.sortData.bind(this)
		this.handleFilterChange = this.handleFilterChange.bind(this)
		this.handleFilterSubmit = this.handleFilterSubmit.bind(this)
		this.handleFilterReset = this.handleFilterReset.bind(this)
		this.handleSearchSubmit = this.handleSearchSubmit.bind(this)
		this.handleSearchChange = this.handleSearchChange.bind(this)
	}

    async componentDidMount() {
		this.loadData()
	}

	async loadData() {
		try {
			let filters = ''
			if(this.state.filter !== '') {
				filters = `?q={"filters":${this.state.filter}}`
			}
			const result = await axios.get(`/v1/api/Climate${filters}`)
			this.setState({
				tableRows: result.data.objects,
				loading: false
			});
		} catch (error) {
			this.setState({
				error,
				loading: false
			});
		}
	}

	changePage = data  => {
        this.setState({
            currentPage: data.selected + 1
        })
    }

	sortData(sortDirection, sortColumn) {
		const columns = {"City": "Climate_idCity", "Precipitation": "Precipitation", "Rain Days": "RainDays", "Average Temperature": "Avg_Temp", "Annual High Temperature": "Annual_High", "Annual Low Temperature": "Annual_Low"}
		let sortedData = []
		if(sortDirection === 'asc') {
			sortedData = this.state.tableRows.sort(function(a, b){return a[columns[sortColumn]]-b[columns[sortColumn]]})
		} else {
			sortedData = this.state.tableRows.sort(function(a, b){return b[columns[sortColumn]]-a[columns[sortColumn]]})
		}

		this.setState({
			tableRows: sortedData,
			currentPage: 1
		})
	}
	
	checkNaNFilters(...args) {
		args.forEach(arg => {
			if(isNaN(Number(arg))) {
				alert(`"${arg}" is not a number`)
				return false
			}
		})
		return true
	}

	handleFilterSubmit(event) {
		if(this.checkNaNFilters(this.state.precipitationValue, this.state.rainDaysValue, this.state.averageTempValue, this.state.annualHighValue, this.state.annualLowValue)) {
			let filter = [{"and": []}]
			if(this.state.precipitationOp !== '' && this.state.precipitationValue !== '') {
				const op = this.state.precipitationOp === "Greater Than" ? "gt" : this.state.precipitationOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Precipitation", "op": op, "val": Number(this.state.precipitationValue)})
			}
			if(this.state.rainDaysOp !== '' && this.state.rainDaysValue !== '') {
				const op = this.state.rainDaysOp === "Greater Than" ? "gt" : this.state.rainDaysOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "RainDays", "op": op, "val": Number(this.state.rainDaysValue)})
			}
			if(this.state.averageTempOp !== '' && this.state.averageTempValue !== '') {
				const op = this.state.averageTempOp === "Greater Than" ? "gt" : this.state.averageTempOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Avg_Temp", "op": op, "val": Number(this.state.averageTempValue)})
			}
			if(this.state.annualHighOp !== '' && this.state.annualHighValue !== '') {
				const op = this.state.annualHighOp === "Greater Than" ? "gt" : this.state.annualHighOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Annual_High", "op": op, "val": Number(this.state.annualHighValue)})
			}
			if(this.state.annualLowOp !== '' && this.state.annualLowValue !== '') {
				const op = this.state.annualLowOp === "Greater Than" ? "gt" : this.state.annualLowOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Annual_Low", "op": op, "val": Number(this.state.annualLowValue)})
			}
			
			this.setState({
				filter: JSON.stringify(filter),
				loading: true
			},() => this.loadData())
		}

		event.preventDefault()
	}

	handleFilterReset(event) {
		this.setState({
			filter: '',
			precipitationValue: '',
			precipitationOp: '',
			rainDaysValue: '',
			rainDaysOp: '',
			averageTempValue: '',
			averageTempOp: '',
			annualHighValue: '',
			annualHighOp: '',
			annualLowValue: '',
			annualLowOp: '',
			search: '',
			searchValues: '',
		}, this.loadData)
		alert("You have reset the table")
	}

	handleFilterChange(event) {
		const target = event.target;
		const name = target.name;
		const value = target.value

		this.setState({
			[name]: value
		});
	}

	handleSearchChange(event) {
		this.setState({
			searchValues: event.target.value
		})
	}

	handleSearchSubmit() {
		const words = this.state.searchValues.split(" ")
		const columns = ["Climate_idCity", "Precipitation", "RainDays", "Avg_Temp", "Annual_High", "Annual_Low"]
		// const cityData = this.state.cityData
		const searchData = this.state.tableRows.filter(row => {
			for(let i = 0; i < words.length; i++) {
				for(let j = 0; j < columns.length; j++) {
					if(columns[j] === "Climate_idCity" && isNaN(Number(words[i]))) {
						if(row["CityName"].toLowerCase().includes(words[i].toLowerCase())) {
							return true
						} else {
							break
						}
					} else if(!isNaN(Number(words[i]))){
						if(row[`${columns[j]}`] === Number(words[i])) {
							return true
						}
					}
				}
			}
			return false
		})

		this.setState({
			tableRows: searchData
		})

	}

	render() {
		if(this.state.loading) {
            return <h2>Loading...</h2>
		}
		
		// Get current rows
        const indexOfLastRow = this.state.currentPage * this.state.tableRowsPerPage
        const indexOfFirstRow = indexOfLastRow - this.state.tableRowsPerPage
        const currentRows = this.state.tableRows.slice(indexOfFirstRow, indexOfLastRow)
        const totalPages = Math.ceil(this.state.tableRows.length / this.state.tableRowsPerPage)

		return (
			
			<div className="climate">
				<br /><br />
				<h1 className="text-center">Climate</h1>
				<br /><br />
				<div className="climate-content">
					<div className="filter mb-3">
					<div className="form-group row mx-1">
							<button className="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
								Filter Search Results
							</button>
							<div className="col-xs-4 ml-5 mr-1">
								<input className="form-control" type="text" placeholder="Search" aria-label="Search" onChange={this.handleSearchChange}/>
							</div>
							<div className="col-xs-4 mr-1">
								<button className="btn btn-primary btn-rounded btn" type="submit" onClick={this.handleSearchSubmit}>Search</button>
							</div>
							<div className="col-xs-4">
								<button className="btn btn-primary btn-rounded btn" type="reset" onClick={this.handleFilterReset}>Reset</button>
							</div>
						</div>
						<div className="collapse" id="collapseFilter">
							<div className="card card-body bg-dark">
								<form className="container" onSubmit={this.handleFilterSubmit} onReset={this.handleFilterReset} >
									<label className="row">
										Precipitation:
										<select className="mx-2" name="precipitationOp" value={this.state.precipitationOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="precipitationValue" value={this.state.precipitationValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Rain Days:
										<select className="mx-2" name="rainDaysOp" value={this.state.rainDaysOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="rainDaysValue" value={this.state.rainDaysValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Average Temperature:
										<select className="mx-2" name="averageTempOp" value={this.state.averageTempOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="averageTempValue" value={this.state.averageTempValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Annual High Temperature:
										<select className="mx-2" name="annualHighOp" value={this.state.annualHighOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="annualHighValue" value={this.state.annualHighValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Annual Low Temperature:
										<select className="mx-2" name="annualLowOp" value={this.state.annualLowOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="annualLowValue" value={this.state.annualLowValue} onChange={this.handleFilterChange} />
									</label>

									<input className="mx-2 my-2" type="submit" value="Submit" />
									<input type="reset" value="Reset" />
								</form>
							</div>
						</div>
					</div>
					<Table tableRows={currentRows} loading={this.state.loading} tableHeader={tableHeader} history={this.props.history} path={'/climate-city'} modelType={'climate'} doSort={true} sortData={this.sortData.bind(this)} searchWords={this.state.searchValues.split(" ")} />
					<div className="page-num">
						<ReactPaginate
							previousLabel={'Previous'}
							nextLabel={'Next'}
							breakLabel={'...'}
							pageCount={totalPages}
							marginPagesDisplayed={2}
							pageRangeDisplayed={3}
							onPageChange={this.changePage}
							forcePage={this.state.currentPage-1}
							breakClassName={'page-item'}
							breakLinkClassName={'page-link'}
							containerClassName={'pagination'}
							pageClassName={'page-item'}
							pageLinkClassName={'page-link'}
							previousClassName={'page-item'}
							previousLinkClassName={'page-link'}
							nextClassName={'page-item'}
							nextLinkClassName={'page-link'}
							activeClassName={'active'}
						/>
					</div>
				</div>
			</div>
		)
	}
}

export default Climate