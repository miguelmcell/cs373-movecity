import React, { useState, useEffect } from "react"
import { geoEqualEarth, geoPath } from "d3-geo"
import { feature } from "topojson-client"

const projection = geoEqualEarth()
  .scale(250)
  .translate([ 800 / 2, 750 / 2 ])
let areas =
[
  {
    "country": "Andorra",
    "coordinates": [
      1.5218,
      42.5075
    ],
    "location": {
        "flag_image": "https://www.worldatlas.com/webimage/flags/countrys/zzzflags/adlarge.gif",
    },
    "name": "Parc Natural de la Vall de Sorteny",
    "area": 10.8
  },
  {
    "country": "Armenia",
    "coordinates": [
      44.509,
      40.1596
    ],
    "location": {
        "flag_image": "http://cdn.wonderfulengineering.com/wp-content/uploads/2015/07/Armenia-Flag-1.jpeg",
    },
    "name": "Sevan",
    "area": 1474.55
  },
  {
    "country": "American Samoa",
    "coordinates": [
      -170.691,
      -14.2846
    ],
    "location": {
        "flag_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Flag_of_American_Samoa.svg/1200px-Flag_of_American_Samoa.svg.png",
    },
    "name": "Amanave Village",
    "area":  0.344291
  },
  {
    "country": "Australia",
    "coordinates": [
      149.129,
      -35.282
    ],
    "location": {
        "flag_image": "http://cdn.wonderfulengineering.com/wp-content/uploads/2015/07/Australia-Flag-28.jpg",
    },
    "name": "Kinchega",
    "area":  445.528604405
  },
  {
    "country": "Austria",
    "coordinates": [
      16.3798,
      48.2201
    ],
    "location": {
        "flag_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Flag_of_Austria.svg/1200px-Flag_of_Austria.svg.png",
    },
    "name": "Naturschutzgebiet Kaisergebirge",
    "area":  92.5828094482422
  },
  {
    "country": "Azerbaijan",
    "coordinates": [
      49.8932,
      40.3834
    ],
    "location": {
        "flag_image": "https://i.ytimg.com/vi/ijiXq9ibokQ/maxresdefault.jpg",
    },
    "name": "Gizilaghaj State Nature Reserve",
    "area":  883.6
  },
  {
    "country": "Belgium",
    "coordinates": [
      4.36761,
      50.8371
    ],
    "location": {
        "flag_image": "https://i.ytimg.com/vi/qsVaNjWdGX0/maxresdefault.jpg",
    },
    "name": "Claire- Fontain",
    "area":  0.469403982162476
  },
  {
    "country": "Bangladesh",
    "coordinates": [
      90.4113,
      23.7055
    ],
    "location": {
        "flag_image": "https://cdn.britannica.com/67/6267-004-D59711CA.jpg",
    },
    "name": "Kaptai",
    "area":  54.64
  },
  {
    "country": "Cyprus",
    "coordinates": [
      33.3736,
      35.1676
    ],
    "location": {
        "flag_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Flag_of_Cyprus.svg/1920px-Flag_of_Cyprus.svg.png",
    },
    "name": "Athalassa",
    "area":  8.70899295806885
  },
  {
    "country": "Czech Republic",
    "coordinates": [
      14.4205,
       50.0878
    ],
    "location": {
        "flag_image": "https://cdn.britannica.com/86/7886-004-297F88A4.jpg",
    },
    "name": "\u0160umava",
    "area":  1671.17
  },
  {
    "country": "Afghanistan",
    "coordinates": [
      69.1761,
      34.5228
    ],
    "location": {
        "flag_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Afghanistan_Flag.jpg/1200px-Afghanistan_Flag.jpg",
      },
      "name": "Pamir-e-Buzurg",
      "area": 1542.0
  },
  {
    "country": "Albania",
    "coordinates": [
      19.8172,
      41.3317
    ],
    "location": {
        "flag_image": "http://cdn.wonderfulengineering.com/wp-content/uploads/2015/07/Albania-Flag-5.png",
    },
    "name": "Mali I Dajtit",
    "area": 247.231002807617
  },
  {
    "country": "Germany",
    "coordinates": [
      13.4115,
      52.5235
    ],
    "location": {
        "flag_image": "https://upload.wikimedia.org/wikipedia/en/thumb/b/ba/Flag_of_Germany.svg/1200px-Flag_of_Germany.svg.png",
    },
    "name": "Federsee",
    "area": 14.0200996398926
  },
  {
    "country": "Denmark",
    "coordinates": [
      12.5681,
      55.6763
    ],
    "location": {
        "flag_image": "https://upload.wikimedia.org/wikipedia/commons/9/96/Flag_of_Denmark_ubt.jpeg",
    },
    "name": "Dybs\u00f8",
    "area": 1.40280199050903
  },
  {
    "country": "Croatia",
    "coordinates": [
      15.9614,
      45.8069
    ],
    "location": {
        "flag_image": "http://justfunfacts.com/wp-content/uploads/2016/02/croatia-flag.jpg",
    },
    "name": "Plitvice Lakes National Park",
    "area": 294.82
  },
  {
    "country": "Indonesia",
    "coordinates": [
      106.83,
      -6.19752
    ],
    "location": {
        "flag_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Flag_of_Indonesia.svg/1200px-Flag_of_Indonesia.svg.png",
    },
    "name": "Bukit Barisan Selatan",
    "area": 2950.0
  },
  {
    "country": "Sri Lanka",
    "coordinates": [
      79.8528,
      6.92148
    ],
    "location": {
        "flag_image": "https://www.graphicmaps.com/r/w1047/images/flags/lk-flag.jpg",
    },
    "name": "Yala Block (1-5)(Ruhuna",
    "area": 136.79
  },
  {
    "country": "Maldives",
    "coordinates": [
      73.5109,
      4.1742
    ],
    "location": {
        "flag_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0f/Flag_of_Maldives.svg/1200px-Flag_of_Maldives.svg.png",
    },
    "name": "Fushifaru Region",
    "area": 14.0
  },
  {
    "country": "Myanmar",
    "coordinates": [
      95.9562,
      21.914
    ],
    "location": {
        "flag_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Flag_of_Myanmar.svg/1200px-Flag_of_Myanmar.svg.png",
    },
    "name": "Shwesettaw W.S",
    "area": 464.28
  },
  {
    "country": "Mongolia",
    "coordinates": [
      106.937,
      47.9129,
    ],
    "location": {
        "flag_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Flag_of_Mongolia.svg/1200px-Flag_of_Mongolia.svg.png",
    },
    "name": "Lxachinvandad mountain",
    "area": 588.
  },
  {
    "country": "Malaysia",
    "coordinates": [
       101.684,
       3.12433
    ],
    "location": {
        "flag_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Flag_of_Malaysia.svg/1200px-Flag_of_Malaysia.svg.png",
    },
    "name": "Similajau",
    "area": 89.96
  },
  {
    "country": "Nepal",
    "coordinates": [
       85.3157,
       27.6939,
    ],
    "location": {
        "flag_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9b/Flag_of_Nepal.svg/1200px-Flag_of_Nepal.svg.png",
    },
    "name": "Chitwan",
    "area":   932.0
  },
  {
    "country": "New Zealand",
    "coordinates": [
       174.776,
        -41.2865
    ],
    "location": {
        "flag_image": "http://media3.s-nbcnews.com/j/newscms/2015_33/1165256/150810-new-zealand-flag-jpo-419a_4f74ffd03fafd40c3c8681257db5da42.nbcnews-ux-2880-1000.jpg",
    },
    "name": "Arthur's Pass National Park",
    "area": 1185.22158419237
  },
  {
    "country": "Pakistan",
    "coordinates": [
      72.8,
      30.5167,
    ],
    "location": {
        "flag_image": "https://i.ytimg.com/vi/GkaeskWmSII/maxresdefault.jpg",
    },
    "name": "Indus River#1",
    "area": 442.0
  },
  {
    "country": "Philippines",
    "coordinates": [
      121.035,
      14.5515,
    ],
    "location": {
        "flag_image": "http://i.ebayimg.com/images/i/191813969538-0-1/s-l1000.jpg",
    },
    "name": "Aurora Memorial",
    "area": 884
  },
  {
    "country": "Palau",
    "coordinates": [
      134.479,
      7.34194
    ],
    "location": {
        "flag_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Flag_of_Palau.svg/1200px-Flag_of_Palau.svg.png",
    },
    "name": "Ngaremeduu Bay Conservation Area",
    "area": 119.681534081
  },
  {
    "country": "Papua New Guinea",
    "coordinates": [
      147.194,
      -9.47357
    ],
    "location": {
        "flag_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Flag_of_Papua_New_Guinea.svg/1920px-Flag_of_Papua_New_Guinea.svg.png",
    },
    "name": "Baiyer River",
    "area":  0.89
  },
  {
    "country": "Georgia",
    "coordinates": [
      44.793,
      41.71
    ],
    "location": {
        "flag_image": "https://www.washingtonpost.com/resizer/UhyS2V-0MJqL9e1eHcvr_ca-VLs=/1484x0/arc-anglerfish-washpost-prod-washpost.s3.amazonaws.com/public/G3VSTGZIA5G4BHQZEOUIKNS4SM.JPG",
    },
    "name": "Mariamjvari",
    "area":   10.4
  },
  {
    "country": "Greece",
    "coordinates": [
       23.7166,
      37.9792,
    ],
    "location": {
        "flag_image": "https://i.ytimg.com/vi/Xqzd5P9wzBM/maxresdefault.jpg",
    },
    "name": "Prespes",
    "area":  51.0240669250488
  },
  {
    "country": "Fiji",
    "coordinates": [
       178.399,
      -18.1149,
    ],
    "location": {
        "flag_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Fiji.svg/1200px-Flag_of_Fiji.svg.png",
    },
    "name": "Naqarabuluti",
    "area":  2.41
  }
]
const WorldMap = () => {
  const [geographies, setGeographies] = useState([]);
  useEffect(() => {
    fetch("/world-110m.json")
      .then(response => {
        if (response.status !== 200) {
          console.log(`There was a problem: ${response.status}`)
          return
        }
        response.json().then(worlddata => {
          setGeographies(feature(worlddata, worlddata.objects.countries).features)
        })
      })
    }, []);
    const [text, setText] = useState("Protected Area:");
    useEffect(() => {
      console.log(text);
    });
    const [flag, setFlag] = useState("");
    useEffect(() => {
      console.log(flag);
    });
  // const [areas,setAreas] = useState([]);
  // useEffect(() => {
  //   for (var i = 1;i<=1;i++){
  //     fetch('https://endangeredanimals.me/api/area?page='+i)
  //     .then(response => response.json())
  //     .then(data => {
  //       data['objects'].forEach(object => {
  //         let builtObject = {};
  //         console.log(object);
  //         builtObject['country'] = object['country'];
  //         builtObject['name'] = object['name'];
  //         builtObject['flag'] = object['location']['flag_image'];
  //         // console.log(object['ecologies'][0]['latitude'] + ', ' + object['ecologies'][0]['longitude']);
  //         let latLong = [object['location']['latitude'],object['location']['longitude']]
  //         builtObject['coordinates'] = latLong;
  //         // let pop = object['population'];
  //         // // console.log(pop);
  //         // let color = '#000000';
  //         // if (pop === 'Increasing'){
  //         //   color = '#7aff5c';
  //         // } else if (pop === 'Decreasing'){
  //         //   color = '#ff0000';
  //         // } else if (pop === 'Stable'){
  //         //   color = '#fff821';
  //         // }
  //         // // console.log(color);
  //         // builtObject['color'] = color;
  //         // builtObject['status'] = 'unknown';
  //         // console.log(builtObject);
  //         areas.push(builtObject);
  //         setAreas(areas)
  //       });
  //       console.log(areas)
  //       // console.log(data['objects'])
  //     });
  //
  //   }
  //
  //   }, [])

  const handleCountryClick = countryIndex => {
    console.log("Clicked on country: ", geographies[countryIndex])
  }

  const handleMarkerClick = i => {
    console.log("Marker: ", areas[i])
    if(areas[i].name == null){
        setText("Protected Area: Unkown");
        setFlag("");
    }
    else{
      setText("Protected Area: " + areas[i].name+'\nCountry: '+areas[i].country+', Area Size: ' +areas[i].area.toFixed(2) +' sq mi');
      console.log(areas[i].location.flag_image);
      setFlag(areas[i].location.flag_image)
    }
  }
  {/* Status: (Increasing,GREEN),(Unknown,BLACK),(STABLE,YELLOW),(Decreasing,RED) */}

  return (
    <div>

      <h4 style={{position: 'absolute',paddingLeft: '1240px',whiteSpace:'pre-wrap'}} >
      {text}
      </h4>
      <img src={flag}  style={{width:'300px',heigh:'300px',position: 'absolute',left: '1450px',paddingTop:'120px'}} >
      </img>
      <svg width={ '100%' } height={ 800 } viewBox="0 0 1000 1000">
        <g className="countries">
          {
            geographies.map((d,i) => (
              <path
                key={ `path-${ i }` }
                d={ geoPath().projection(projection)(d) }
                className="country"
                fill={ `rgba(255,255,255,0.2)` }
                stroke="#000000"
                strokeWidth={ 0.5 }
                onClick={ () => handleCountryClick(i) }
              />
            ))
          }
        </g>
        <g className="markers">
          {
            areas.map((area, i) => (
              <circle
                key={ `marker-${i}` }
                cx={ projection(area.coordinates)[0] }
                cy={ projection(area.coordinates)[1] }
                r={6}
                fill='#FF0000'
                stroke="#FFFFFF"
                className="marker"
                onMouseOver={ () => handleMarkerClick(i) }
              />
            ))
          }
        </g>
      </svg>
    </div>
  )
}

export default WorldMap
