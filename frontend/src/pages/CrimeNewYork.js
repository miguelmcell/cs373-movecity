import React, {Component} from 'react'
import crimeImg from '../img/crime-pic.jpg'

export default class CrimeNewYork extends Component {

	render() {
		return (
			<div className="text-center">
				<br /><br />
				<h1 className="text-center">New York City Crime</h1>
				<br /><br />
                <img src={crimeImg} width="100%" height="400px"/>
				<h3>Aggravated Assault: 30546</h3>	
				<h3>Arson: 0</h3>
				<h3>Burglary: 14098</h3>
				<h3>Homicide: 352</h3>
				<h3>Human Trafficing: 0</h3>
				<h3>Larceny: 108376</h3>
				<h3>Property Crime: 7386</h3>
				<h3>Vehicle Theft: 129860</h3>
				<h3>Rape: 2244</h3>
				<h3>Robbery: 16946</h3>
				<h3>Violent Crime: 50088</h3>
				<h3>New York Climate data: <a href="/climate-new-york">click here</a></h3>
				<h3>New York Quality of Life data: <a href="/quality-of-life-new-york">click here</a></h3>
            </div>
		)
	}
}
