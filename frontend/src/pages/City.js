import React, {Component} from 'react'

export default class City extends Component {
	constructor(props) {
		super(props)
		this.state = {
			city: [],
			climate: [],
			qol: [],
			crime: []
		}
	}

	// TODO: componentdidmount get the city names
	//       onclick then get data to pass into instance page

	render() {
		return (
			<div className="container">
				<h1 className="text-center mt-5">City Data</h1>
				<table className="table table-striped table-dark">
					<thead>
						<tr>
							<th scope="col">City</th>
							<th scope="col">Quality Of Life</th>
							<th scope="col">Climate</th>
							<th scope="col">Crime</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td scope="row">Los Angeles</td>
							<td><a href="/quality-of-life-los-angeles">Quality Of Life</a></td>
							<td><a href="/climate-los-angeles">Climate</a></td>
							<td><a href="/crime-los-angeles">Crime</a></td>
						</tr>
						<tr>
							<td scope="row">New York</td>
							<td><a href="/quality-of-life-new-york">Quality Of Life</a></td>
							<td><a href="/climate-new-york">Climate</a></td>
							<td><a href="/crime-new-york">Crime</a></td>
						</tr>
						<tr>
							<td scope="row">Chicago</td>
							<td><a href="/quality-of-life-chicago">Quality Of Life</a></td>
							<td><a href="/climate-chicago">Climate</a></td>
							<td><a href="/crime-chicago">Crime</a></td>
						</tr>
					</tbody>
				</table>
			</div>
		)
	}
}