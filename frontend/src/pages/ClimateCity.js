import React, { Component } from 'react'
import axios from 'axios'

class ClimateCity extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            imageURL: '',
            cityData: [],
        }
        this.onClickQOL = this.onClickQOL.bind(this)
        this.onClickCrime = this.onClickCrime.bind(this)
    }

    async componentDidMount() {
		try {
            const path = "v1/getCityImage/" + String(this.props.location.state.city) + ",%20" + String(this.props.location.state.state)
			const result = await axios.get(path)
			this.setState({
				imageURL: result.data,
				isLoading: false
			});
		} catch (error) {
			this.setState({
				error,
                isLoading: false,
			});
        }

    }
    
    async onClickQOL(){
        try {
			const result = await axios.get('/v1/api/QOL')
            for(var i = 0; i < result.data.objects.length; i++) {
                if(result.data.objects[i].QOL_idCity === this.props.location.state.id) {
                    this.props.history.push({pathname: '/quality-of-life-city', state: { data: result.data.objects[i], city: this.props.location.state.city, state: this.props.location.state.state, id: this.props.location.state.id }})
                    break
                }
            }
			
		} catch (error) {
			this.setState({
				error,
                isLoading: false,
			});
		}
    }
    
    async onClickCrime(){
		try {
			const result = await axios.get('/v1/api/Crime')
            for(var i = 0; i < result.data.objects.length; i++) {
                if(result.data.objects[i].Crime_idCity === this.props.location.state.id) {
                    this.props.history.push({pathname: '/crime-city', state: { data: result.data.objects[i], city: this.props.location.state.city, state: this.props.location.state.state, id: this.props.location.state.id }})
                    break
                }
            }
			
		} catch (error) {
			this.setState({
				error,
                isLoading: false,
			});
		}
	}

    render() {
       // checking if there is no reference for what city they want to see
       try {
        const data = this.props.location.state.data
    } catch(error) {
        return (
            <div>
                There is no data to be displayed. Please use the homepage or navbar to redirect to
                this page.
            </div>
        )
    }

    if(this.state.isLoading) {
        return <div>Loading image...</div>
    }

        return(
            <div className="container">
                <div className="row">
                    <img src={this.state.imageURL} width="100%" height="400px"/>
                </div>
                <div className="row mt-5">
                    {/* <p>{JSON.stringify(this.props.location.state.data)}</p> */}
                    <table className="table table-striped table-dark">
                        <thead>
                            <tr>
                                <th scope="col">City</th>
                                <th scope="col">Precipitation</th>
                                <th scope="col">Rain Days</th>
                                <th scope="col">Average Temperature</th>
                                <th scope="col">Annual High</th>
                                <th scope="col">Annual Low</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">{this.props.location.state.city}</td>
                                <td>{this.props.location.state.data.Precipitation} mm</td>
                                <td>{this.props.location.state.data.RainDays} per year</td>
                                <td>{this.props.location.state.data.Avg_Temp}°C</td>
                                <td>{this.props.location.state.data.Annual_High}°C</td>
                                <td>{this.props.location.state.data.Annual_Low}°C</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div className="row d-flex justify-content-center">
                    <button type="button" className="btn btn-secondary mx-5 my-5" onClick={this.onClickQOL}>{this.props.location.state.city} Quality Of Life Data</button>
                    <button type="button" className="btn btn-secondary mx-5 my-5" onClick={this.onClickCrime}>{this.props.location.state.city} Crime Data</button>
                    <button type="button" className="btn btn-secondary mx-5 my-5">All City Datas</button>
                </div>
            </div>
        )
    }

}

export default ClimateCity