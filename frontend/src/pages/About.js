import React, {
	Component
} from 'react';
import mehrdadImg from '../img/mehrdad.jpg';
import miguelImg from '../img/miguel.jpeg';
import justinImg from '../img/justin.jpg';
import adhithyaImg from '../img/adhithya.jpg';
import gageImg from '../img/gage.jpg';
import reactImg from '../img/React-logo.svg';
import bootstrapImg from '../img/bootstrap-logo.png';
import awsImg from '../img/aws-logo.png';
import postmanImg from '../img/postman-logo.png';
import flaskImg from '../img/flask.png';
import {Container,Row,Col} from 'react-bootstrap';
// import requests from '../util/requests.js';
import {
	declareTypeAlias
} from '@babel/types';

export default class About extends Component {
	constructor(props) {
		super(props);
		this.state = {
			total_issues: 0,
			total_commits: 0,

			adhithya_commits: 0,
			adhithya_issues: 0,
			adhithya_unit_tests: 0,

			miguel_commits: 0,
			miguel_issues: 0,
			miguel_unit_tests: 30,

			gage_commits: 0,
			gage_issues: 0,
			gage_unit_tests: 0,

			justin_commits: 0,
			justin_issues: 0,
			justin_unit_tests: 0,

			mehrdad_commits: 0,
			mehrdad_issues: 0,
			mehrdad_unit_tests: 0
		};
	}
	getUserIssues(axios) {
	  let config = {
	    headers: {
	      "PRIVATE-TOKEN": "ic7tQSERwW4SnsdxXfuD",
	    }
	  }
	  let count = [0, 0, 0, 0, 0];
	  let data = {}
	  axios.get("https://gitlab.com/api/v4/projects/14523582/issues", data, config)
	    .then(function (response) {
	      // Grab commits with 100 commits per page
	      var numPages = parseInt(response["headers"]["x-total"]) / 100 + 1;
	      numPages = parseInt(numPages)
	      var i = 1;
	      for (i; i < numPages + 1; i += 1) {
	        var url = "https://gitlab.com/api/v4/projects/14523582/issues?per_page=100&page=" + i;
	        axios.get(url, data, config)
	          .then(function (request) {
	            request["data"].forEach(function (element) {
	              element["assignees"].forEach(function (assignee) {
	                if (assignee["name"].toLowerCase().includes("balaji")) {
	                  count[0] += 1;
	                } else if (assignee["name"].toLowerCase().includes("mendoza")) {
	                  count[1] += 1;
	                } else if (assignee["name"].toLowerCase().includes("courtaway")) {
	                  count[2] += 1;
	                } else if (assignee["name"].toLowerCase().includes("zhang")) {
	                  count[3] += 1;
	                } else if (assignee["name"].toLowerCase().includes("darraji")) {
	                  count[4] += 1;
	                }
	              });
	            });
	            this.setState(state => ({
	              adhithya_issues: count[0],
	              miguel_issues: count[1],
	              gage_issues: count[2],
	              justin_issues: count[3],
	              mehrdad_issues: count[4]
	            }));
	          }.bind(this))
	      }
	    }.bind(this))
	    .catch(function (error) {
	      // handle error
	      console.log(error);
	    })
	    .finally(function () {
	      // always executed)
	    });
	}
	getAllIssues = (axios) => {
	  let config = {
	    headers: {
	      "PRIVATE-TOKEN": "ic7tQSERwW4SnsdxXfuD",
	    }
	  }

	  let data = {}

	  axios.get("https://gitlab.com/api/v4/projects/14523582/issues", data, config)
	    .then(function (response) {
	      // handle success
	      this.setState(state => ({
	        total_issues: response.headers["x-total"]
	      }));
	    }.bind(this))
	    .catch(function (error) {
	      // handle error
	      console.log(error);
	    })
	    .finally(function () {
	      // always executed

	    });
	}
	getAllCommits = (axios) => {
	  let config = {
	    headers: {
	      "PRIVATE-TOKEN": "ic7tQSERwW4SnsdxXfuD",
	    }
	  }

	  let data = {}

	  axios.get("https://gitlab.com/api/v4/projects/14523582/repository/commits/", data, config)
	    .then(function (response) {
	      // handle success
	      this.setState(state => ({
	        total_commits: response.headers["x-total"]
	      }));
	    }.bind(this))
	    .catch(function (error) {
	      // handle error
	      console.log(error);
	    })
	    .finally(function () {
	      // always executed
	    });
	}
	getUserCommits(axios) {
	  let config = {
	    headers: {
	      "PRIVATE-TOKEN": "ic7tQSERwW4SnsdxXfuD",
	    }
	  }
	  let count = [0, 0, 0, 0, 0];
	  let data = {}
	  axios.get("https://gitlab.com/api/v4/projects/14523582/repository/commits/", data, config)
	    .then(function (response) {
	      // Grab commits with 100 commits per page
	      var numPages = parseInt(response["headers"]["x-total"]) / 100 + 1;
	      numPages = parseInt(numPages)
	      var i = 1;
	      for (i; i < numPages + 1; i += 1) {
	        var url = "https://gitlab.com/api/v4/projects/14523582/repository/commits?per_page=100&page=" + i;
	        axios.get(url, data, config)
	          .then(function (request) {
	            request["data"].forEach(function (element) {
	              if (element["committer_name"].toLowerCase().includes("laser8000")) {
	                count[0] += 1;
	              } else if (element["committer_name"].toLowerCase().includes("miguel")) {
	                count[1] += 1;
	              } else if (element["committer_name"].toLowerCase().includes("mendoza")) {
	                count[1] += 1;
	              } else if (element["committer_name"].toLowerCase().includes("ubuntu")) {
	                count[1] += 1;
	              } else if (element["committer_name"].toLowerCase().includes("courtaway")) {
	                count[2] += 1;
	              } else if (element["committer_name"].toLowerCase().includes("zhang")) {
	                count[3] += 1;
	              } else if (element["committer_name"].toLowerCase().includes("darraji")) {
	                count[4] += 1;
	              }
	            });
	            this.setState(state => ({
	              adhithya_commits: count[0],
	              miguel_commits: count[1],
	              gage_commits: count[2],
	              justin_commits: count[3],
	              mehrdad_commits: count[4]
	            }));
	          }.bind(this))
	      }
	    }.bind(this))
	    .catch(function (error) {
	      // handle error
	      console.log(error);
	    })
	    .finally(function () {
	      // always executed)
	    });
	}
	componentDidMount() {
		const axios = require('axios').default;
		// const requests = require('../util/requests')(axios);
		this.getAllCommits(axios)
		this.getAllIssues(axios);
		this.getUserIssues(axios)
		this.getUserCommits(axios);
		// requests.hitTestEndpoint(axios).then(function(result){
		// 	console.log(result);
		// });

	}

	render() {
		return (
			<div>
				<br /><br />
				<h1 className="text-center">About Us</h1>
				<div className="container">
					<div className="row mt-5 justify-content-center">
						<div className="card bg-dark mx-3 mb-3" style={{width: "20rem"}}>
							<img className="card-img-top" src={adhithyaImg} alt="adhithya" />
							<div className="card-block">
								<h3 className="text-center card-title text-center"><br />Adhithya Balaji</h3>
								<h4 className="text-center card-text">Bio: Student at UT Austin, BS in CS, grad Spring 2020 <br />Responsibilities: Back-End<br />Commits: {this.state.adhithya_commits}<br />Issues: {this.state.adhithya_issues} <br />Unit Tests: {this.state.adhithya_unit_tests}</h4>
							</div>
						</div>
						<div className="card bg-dark mx-3 mb-3" style={{width: "20rem"}}>
							<img className="card-img-top" src={gageImg} alt="gage" />
							<div className="card-block">
								<h3 className="text-center card-title text-center"><br />Gage Courtaway</h3>
								<h4 className="text-center card-text">Bio: Student at UT Austin, BS in CS, grad Spring 2020 <br />Responsibilities: Back-End<br />Commits: {this.state.gage_commits} <br />Issues: {this.state.gage_issues}<br />Unit Tests: {this.state.gage_unit_tests} </h4>
							</div>
						</div>
						<div className="card bg-dark mx-3 mb-3" style={{width: "20rem"}}>
							<img className="card-img-top" src={mehrdadImg} alt="mehrdad" />
							<div className="card-block">
								<h3 className="text-center card-title text-center"><br />Mehrdad Darraji</h3>
								<h4 className="text-center card-text">Bio: Student at UT Austin, BS in CS, grad Spring 2020 <br />Responsibilities: Front-End<br />Commits: {this.state.mehrdad_commits}<br />Issues: {this.state.mehrdad_issues}<br />Unit Tests: {this.state.mehrdad_unit_tests}</h4>
							</div>
						</div>
						<div className="card bg-dark mx-3 mb-3" style={{width: "20rem"}}>
							<img className="card-img-top" src={miguelImg} alt="miguel" />
							<div className="card-block">
								<h3 className="text-center card-title text-center"><br />Miguel Mendoza</h3>
								<h4 className="text-center card-text">Bio: Student at UT Austin, BS in CS, grad Spring 2020 <br />Responsibilities: Back-End/Dev Ops<br />Commits: {this.state.miguel_commits}<br />Issues: {this.state.miguel_issues}<br />Unit Tests: {this.state.miguel_unit_tests}</h4>
							</div>
						</div>
						<div className="card bg-dark mx-3 mb-3" style={{width: "20rem", height: "20rem"}}>
							<div className="card-block">
								<h2 className="card-text text-center"><br /><br />Total Commits: {this.state.total_commits}<br />Total Issues: {this.state.total_issues}<br />Total Unit Tests: 30</h2>
							</div>
						</div>
						<div className="card bg-dark mx-3 mb-3" style={{width: "20rem"}}>
							<img className="card-img-top" src={justinImg} alt="justin" />
							<div className="card-block">
								<h3 className="card-title text-center"><br />Justin Zhang</h3>
								<h4 className="text-center card-text">Bio: Student at UT Austin, BS in CS, grad Spring 2021 <br />Responsibilities: Dev Ops<br />Commits: {this.state.justin_commits}<br />Issues: {this.state.justin_commits}<br />Unit Tests: {this.state.justin_unit_tests}</h4>
							</div>
						</div>
					</div>
					<h1 className="text-center"><br />Tools Used</h1>
					<br />
					<Row align="center">
					<Col>
					<img src={reactImg} width="300px" />
					</Col>
					<Col>
					<img src={bootstrapImg} width="200px" />
					</Col>
					<Col>
					<img src={awsImg} width="200px" />
					</Col>
					<Col>
					<img src={postmanImg} width="200px" />
					</Col>
					<Col>
					<img src={flaskImg} height="200px" width="160px" />
					</Col>
					</Row>
					<br /><br />

				</div>

				<div>
				<h1 className="text-center"><br />Data Sources</h1>

				<p className="text-center"><a href="https://developers.google.com/maps/documentation">Google Maps API</a></p>
				<p className="text-center"><a href="https://crime-data-explorer.fr.cloud.gov/api">FBI Crime Data API</a></p>
				<p className="text-center"><a href="https://datahelpdesk.worldbank.org/knowledgebase/articles/902061-climate-data-api">World Climate API</a></p>
				<p className="text-center"><a href="https://developers.teleport.org/">Quality of Living</a></p>

				</div>

				<div>
				<h1 className="text-center"><br />Links</h1>
				<h3 className="text-center"><br /><a href="https://gitlab.com/miguelmcell/cs373-movecity">GitLab Repository</a></h3>
				<br /><br /><br />
				</div>
			</div>
		);
	}
}
