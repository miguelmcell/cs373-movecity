import React, {Component} from 'react'
import climateImg from '../img/cali-heatmap.gif'

export default class ClimateLosAngeles extends Component {

	render() {
		return (
			<div className="text-center">
				<br /><br />
				<h1 className="text-center">Los Angeles Climate</h1>
				<br /><br />
                <img src={climateImg} width="50%" height="400px"/>
				<h3>Precipitation: 16 inches</h3>
				<h3>Snow: 0 inches</h3>
				<h3>Average Temperature: 63.8°F</h3>
				<h3>Annual High Temperature: 71.7°F</h3>
				<h3>Annual Low Temperature: 55.9°F</h3>
				<h3>Los Angeles Crime data: <a href="/crime-los-angeles">click here</a></h3>
				<h3>Los Angeles Quality of Life data: <a href="/quality-of-life-los-angeles">click here</a></h3>
            </div>
		)
	}
}
