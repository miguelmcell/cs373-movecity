import React, { Component } from 'react'
import axios from 'axios'

class CrimeCity extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            imageURL: ''
        }

        this.onClickQOL = this.onClickQOL.bind(this)
        this.onClickClimate = this.onClickClimate.bind(this)
    }

    async componentDidMount() {
		try {
			const path = "v1/getCityImage/" + String(this.props.location.state.city) + ",%20" + String(this.props.location.state.state)
			const result = await axios.get(path)
			this.setState({
				imageURL: result.data,
				isLoading: false
			});
		} catch (error) {
			this.setState({
				error,
                isLoading: false,
			});
		}
    }
    
    async onClickClimate(){
		try {
			const result = await axios.get('/v1/api/Climate')
            for(var i = 0; i < result.data.objects.length; i++) {
                if(result.data.objects[i].Climate_idCity === this.props.location.state.id) {
                    this.props.history.push({pathname: '/climate-city', state: { data: result.data.objects[i], city: this.props.location.state.city, state: this.props.location.state.state, id: this.props.location.state.id }})
                    break
                }
            }
			
		} catch (error) {
			this.setState({
				error,
                isLoading: false,
			});
		}
    }
    
    async onClickQOL(){
		try {
			const result = await axios.get('/v1/api/QOL')
            for(var i = 0; i < result.data.objects.length; i++) {
                if(result.data.objects[i].QOL_idCity === this.props.location.state.id) {
                    this.props.history.push({pathname: '/quality-of-life-city', state: { data: result.data.objects[i], city: this.props.location.state.city, state: this.props.location.state.state, id: this.props.location.state.id }})
                    break
                }
            }
			
		} catch (error) {
			this.setState({
				error,
                isLoading: false,
			});
		}
	}

    render() {
       // checking if there is no reference for what city they want to see
       try {
        const data = this.props.location.state.data
    } catch(error) {
        return (
            <div>
                There is no data to be displayed. Please use the homepage or navbar to redirect to
                this page.
            </div>
        )
    }

    if(this.state.isLoading) {
        return <div>Loading image...</div>
    }

        return(
            <div className="container">
                <div className="row">
                    <img src={this.state.imageURL} width="100%" height="400px"/>
                </div>
                <div className="row mt-5">
                    {/* <p>{JSON.stringify(this.props.location.state.data)}</p> */}
                    <table className="table table-striped table-dark">
                        <thead>
                            <tr>
                                <th scope="col">City</th>
                                <th scope="col">Aggravated Assault</th>
                                <th scope="col">Arson</th>
                                <th scope="col">Burglary</th>
                                <th scope="col">Larceny</th>
                                <th scope="col">Property Crime</th>
                                <th scope="col">Vehicle Theft</th>
                                <th scope="col">Robbery</th>
                                <th scope="col">Violent Crime</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">{this.props.location.state.city}</td>
                                <td>{this.props.location.state.data.Aggravated_Assault} cases in 2018</td>
                                <td>{this.props.location.state.data.Arson} cases in 2018</td>
                                <td>{this.props.location.state.data.Burglary} cases in 2018</td>
                                <td>{this.props.location.state.data.Larceny} cases in 2018</td>
                                <td>{this.props.location.state.data.Property_Crime} cases in 2018</td>
                                <td>{this.props.location.state.data.Vehicle_Theft} cases in 2018</td>
                                <td>{this.props.location.state.data.Robbery} cases in 2018</td>
                                <td>{this.props.location.state.data.Violent_Crime} cases in 2018</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div className="row d-flex justify-content-center">
                    <button type="button" className="btn btn-secondary mx-5 my-5" onClick={this.onClickQOL}>{this.props.location.state.city} Quality of Life Data</button>
                    <button type="button" className="btn btn-secondary mx-5 my-5" onClick={this.onClickClimate}>{this.props.location.state.city} Climate Data</button>
                    <button type="button" className="btn btn-secondary mx-5 my-5">All City Datas</button>
                </div>
            </div>
        )
    }

}

export default CrimeCity