import React, {Component} from 'react'
import crimeImg from '../img/crime-pic.jpg'

export default class CrimeChicago extends Component {

	render() {
		return (
			<div className="text-center">
				<br /><br />
				<h1 className="text-center">Chicago Crime</h1>
				<br /><br />
                <img src={crimeImg} width="100%" height="400px"/>
				<h3>Aggravated Assault: 13094</h3>
				<h3>Arson: 536</h3>
				<h3>Burglary: 13169</h3>
				<h3>Homicide: 481</h3>
				<h3>Human Trafficing: 0</h3>
				<h3>Larceny: 57136</h3>
				<h3>Property Crime: 10200</h3>
				<h3>Vehicle Theft: 80505</h3>
				<h3>Rape: 1482</h3>
				<h3>Robbery: 9641</h3>
				<h3>Violent Crime: 24698</h3>
				<h3>Chicago Climate data: <a href="/climate-chicago">click here</a></h3>
				<h3>Chicago Quality of Life data: <a href="/quality-of-life-chicago">click here</a></h3>
            </div>
		)
	}
}
