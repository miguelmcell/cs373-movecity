import React, {Component} from 'react'
import Geocode from 'react-geocode'
import { Map, GoogleApiWrapper } from 'google-maps-react';
// import qolImg from '../img/LA-QOL.jpg'

Geocode.setApiKey("AIzaSyBg4GQQdNSceQbeOmVi5hpbYlAi7FIQvJc");
Geocode.setLanguage("en");



const mapStyles = {
  width: '50%',
  height: '50%'
};

export class QualityOfLifeLosAngeles extends Component {
	constructor(props) {
		super(props);
		this.state = {
			latitude: 0,
			longitude: 0,
			isLoading: true
		};
		// this.getCityPos = this.getCityPos.bind(this);
	}
	componentDidMount() {
		Geocode.fromAddress("Los Angeles").then(
			response => {
		    const { lat, lng } = response.results[0].geometry.location;
		    console.log(lat, lng);
				this.setState(state => ({
					latitude: lat,
					longitude: lng,
					isLoading: false
				}));
		  },
		  error => {
		    console.error(error);
		  }
		);
	}
	render() {
		if (this.state.isLoading) return null;
		return (
			<div className="text-center">
				<br /><br />
				<h1 className="text-center">Los Angeles Quality of Life</h1>
				<br /><br />
				<Map
	        google={this.props.google}
	        zoom={10}
	        style={mapStyles}
	        initialCenter={{
					 lat: this.state.latitude,
					 lng: this.state.longitude
					}}
      	/>
				<br /><br />
				<h3>Housing: 1.5</h3>
				<h3>Cost of Living: 4.5</h3>
				<h3>Commute: 3.6</h3>
				<h3>Healthcare: 8.5</h3>
				<h3>Education: 8.6</h3>
				<h3>Environmental Quality: 4.7</h3>
				<h3>Tax: 4.8</h3>
				<h3>Economy: 6.5</h3>
				<h3>Internet Access: 5.5</h3>
				<h3>Safety: 5.7</h3>
				<h3>Los Angeles Climate data: <a href="/climate-los-angeles">click here</a></h3>
				<h3>Los Angeles Crime data: <a href="/crime-los-angeles">click here</a></h3>
            </div>
		)
	}
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyBg4GQQdNSceQbeOmVi5hpbYlAi7FIQvJc'
})(QualityOfLifeLosAngeles);
