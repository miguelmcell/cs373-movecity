import React, {Component} from 'react'
import qolImg from '../img/CHIC-QOL.jpg'

export default class QualityOfLifeChicago extends Component {

	render() {
		return (
			<div className="text-center">
				<br /><br />
				<h1 className="text-center">Chicago Quality of Life</h1>
				<br /><br />
                <img src={qolImg} width="100%" height="400px"/>
				<h3>Housing: 3.7</h3>	
				<h3>Cost of Living: 4.8</h3>
				<h3>Commute: 4.6</h3>
				<h3>Healthcare: 8.6</h3>
				<h3>Education: 8</h3>
				<h3>Environmental Quality: 6.8</h3>
				<h3>Tax: 4</h3>
				<h3>Economy: 6.5</h3>
				<h3>Internet Access: 6.6</h3>
				<h3>Safety: 3.8</h3>
				<h3>Chicago Climate data: <a href="/climate-chicago">click here</a></h3>
				<h3>Chicago Crime data: <a href="/crime-chicago">click here</a></h3>
            </div>
		)
	}
}
