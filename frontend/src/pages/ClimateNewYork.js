import React, {Component} from 'react'
import climateImg from '../img/ny-heatmap.png'

export default class ClimateNewYork extends Component {

	render() {
		return (
			<div className="text-center">
				<br /><br />
				<h1 className="text-center">New York Climate</h1>
				<br /><br />
                <img src={climateImg} width="50%" height="400px"/>
				<h3>Precipitation: 45 inches</h3>
				<h3>Snow: 25 inches</h3>
				<h3>Average Temperature: 55.15°F</h3>
				<h3>Annual High Temperature: 62.3°F</h3>
				<h3>Annual Low Temperature: 48°F</h3>
				<h3>New York Crime data: <a href="/crime-new-york">click here</a></h3>
				<h3>New York Quality of Life data: <a href="/quality-of-life-new-york">click here</a></h3>
            </div>
		)
	}
}
