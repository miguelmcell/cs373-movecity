import React, {Component} from 'react'
import axios from 'axios'
import Table from '../components/Table'
import ReactPaginate from 'react-paginate'

const tableHeaderQOL = ["City", "Housing", "Cost of Living", "Commute", "Healthcare", "Education", "Environmental Quality", "Tax"]
const tableHeaderCrime = ["City", "Aggravated Assault", "Arson", "Burglary", "Homicide", "Larceny", "Property Crime", "Vehicle Theft", "Robbery", "Violent Crime"]
const tableHeaderClimate = ["City", "Precipitation", "Rain Days", "Average Temperature", "Annual High Temperature", "Annual Low Temperature"]

class SearchPage extends Component {
    constructor() {
        super()
        this.state = {
            qolData: [],
            crimeData: [],
            climateData: [],
            cityData: [],
            tableRowsPerPage: 10,
            currentPageQOL: 1,
            currentPageCrime: 1,
            currentPageClimate: 1,
            loading: true
        }
        this.handleSearchSubmit = this.handleSearchSubmit.bind(this)
    }

    async componentDidMount() {
        try {
			const qolResult = await axios.get('/v1/api/QOL')
            const crimeResult = await axios.get('/v1/api/Crime')
            const climateResult = await axios.get('/v1/api/Climate')
            const cityResult = await axios.get('/v1/api/City')
			this.setState({
                qolData: qolResult.data.objects,
                crimeData: crimeResult.data.objects,
                climateData: climateResult.data.objects,
                cityData: cityResult.data.objects,
			}, this.handleSearchSubmit)
		} catch (error) {
			this.setState({
				error,
				loading: false
			})
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.location.state.searchValues !== prevProps.location.state.searchValues) {
          this.handleSearchSubmit()
        }
      }

    handleSearchSubmit() {
        const words = this.props.location.state.searchValues.split(" ")
        const cityData = this.state.cityData

		const crimeColumns = ["Crime_idCity", "Aggravated_Assault", "Arson", "Burglary", "Homicide", "Larceny", "Property_Crime", "Vehicle_Theft", "Robbery", "Violent_Crime"]
		const searchCrimeData = this.state.crimeData.filter(row => {
			for(let i = 0; i < words.length; i++) {
				for(let j = 0; j < crimeColumns.length; j++) {
					if(crimeColumns[j] === "Crime_idCity" && isNaN(Number(words[i]))) {
						for(let k = 0; k < cityData.length; k++) {
							if(cityData[k]["idCity"] === row["Crime_idCity"]) {
								if(cityData[k]["CityName"].toLowerCase().includes(words[i].toLowerCase())) {
									return true
								} else {
									break
								}
							}
						}
					} else if(!isNaN(Number(words[i]))){
						if(row[`${crimeColumns[j]}`] === Number(words[i])) {
							return true
						}
					}
				}
			}
			return false
        })
        
        const climateColumns = ["Climate_idCity", "Precipitation", "RainDays", "Avg_Temp", "Annual_High", "Annual_Low"]
		const searchClimateData = this.state.climateData.filter(row => {
			for(let i = 0; i < words.length; i++) {
				for(let j = 0; j < climateColumns.length; j++) {
					if(climateColumns[j] === "Climate_idCity" && isNaN(Number(words[i]))) {
						for(let k = 0; k < cityData.length; k++) {
							if(cityData[k]["idCity"] === row["Climate_idCity"]) {
								if(cityData[k]["CityName"].toLowerCase().includes(words[i].toLowerCase())) {
									return true
								} else {
									break
								}
							}
						}
					} else if(!isNaN(Number(words[i]))){
						if(row[`${climateColumns[j]}`] === Number(words[i])) {
							return true
						}
					}
				}
			}
			return false
        })
        
        const qolColumns = ["QOL_idCity", "Housing", "Cost_of_Living", "Commute", "Healthcare", "Education", "Environmental_Quality", "Tax"]
		const searchQOLData = this.state.qolData.filter(row => {
			for(let i = 0; i < words.length; i++) {
				for(let j = 0; j < qolColumns.length; j++) {
					if(qolColumns[j] === "QOL_idCity" && isNaN(Number(words[i]))) {
						for(let k = 0; k < cityData.length; k++) {
							if(cityData[k]["idCity"] === row["QOL_idCity"]) {
								if(cityData[k]["CityName"].toLowerCase().includes(words[i].toLowerCase())) {
									return true
								} else {
									break
								}
							}
						}
					} else if(!isNaN(Number(words[i]))){
						if(row[`${qolColumns[j]}`] === Number(words[i])) {
							return true
						}
					}
				}
			}
			return false
		})

		this.setState({
            qolData: searchQOLData,
            climateData: searchClimateData,
            crimeData: searchCrimeData,
            loading: false,
		})

    }
    
    changePageQOL = data  => {
        this.setState({
            currentPageQOL: data.selected + 1
        })
    }

    changePageCrime = data  => {
        this.setState({
            currentPageCrime: data.selected + 1
        })
    }

    changePageClimate = data  => {
        this.setState({
            currentPageClimate: data.selected + 1
        })
    }

    render() {
        // checking if there is no reference for what they searched
        try {
            const data = this.props.location.state.searchValues
        } catch(error) {
            return (
                <div>
                    There is no data to be displayed. Please use the homepage or navbar to redirect to
                    this page.
                </div>
            )
        }

        if(this.state.loading) {
            return <div>Loading...</div>
        }

        // Get current rows for 3 models
        const indexOfLastRowQOL = this.state.currentPageQOL * this.state.tableRowsPerPage
        const indexOfFirstRowQOL = indexOfLastRowQOL - this.state.tableRowsPerPage
        const currentRowsQOL = this.state.qolData.slice(indexOfFirstRowQOL, indexOfLastRowQOL)
        const totalPagesQOL = Math.ceil(this.state.qolData.length / this.state.tableRowsPerPage)
        
        const indexOfLastRowCrime = this.state.currentPageCrime * this.state.tableRowsPerPage
        const indexOfFirstRowCrime = indexOfLastRowCrime - this.state.tableRowsPerPage
        const currentRowsCrime = this.state.crimeData.slice(indexOfFirstRowCrime, indexOfLastRowCrime)
        const totalPagesCrime = Math.ceil(this.state.crimeData.length / this.state.tableRowsPerPage)
        
        const indexOfLastRowClimate = this.state.currentPageClimate * this.state.tableRowsPerPage
        const indexOfFirstRowClimate = indexOfLastRowClimate - this.state.tableRowsPerPage
        const currentRowsClimate = this.state.climateData.slice(indexOfFirstRowClimate, indexOfLastRowClimate)
		const totalPagesClimate = Math.ceil(this.state.climateData.length / this.state.tableRowsPerPage)

        return (
            <div className="container">
                {/* <p>{this.props.location.state.searchValues}</p>
                <p>{this.state.qolData.length}</p>
                <p>{this.state.crimeData.length}</p>
                <p>{this.state.climateData.length}</p> */}
                <h1 className="text-center mt-5">Search Results</h1>
                <div className="row mt-5">
                    <h3 className="text-center mb-3">Quality Of Life</h3>
                    <Table tableRows={currentRowsQOL} loading={this.state.loading} tableHeader={tableHeaderQOL} history={this.props.history} path={'/quality-of-life-city'} modelType={'quality-of-life'} doSort={false} searchWords={this.props.location.state.searchValues.split(" ")} />
                    <ReactPaginate
							previousLabel={'Previous'}
							nextLabel={'Next'}
							breakLabel={'...'}
							pageCount={totalPagesQOL}
							marginPagesDisplayed={2}
							pageRangeDisplayed={3}
							onPageChange={this.changePageQOL}
							forcePage={this.state.currentPageQOL-1}
							breakClassName={'page-item'}
							breakLinkClassName={'page-link'}
							containerClassName={'pagination'}
							pageClassName={'page-item'}
							pageLinkClassName={'page-link'}
							previousClassName={'page-item'}
							previousLinkClassName={'page-link'}
							nextClassName={'page-item'}
							nextLinkClassName={'page-link'}
							activeClassName={'active'}
						/>
                </div>
                <div className="row mt-5">
                    <h3 className="text-center mb-3">Climate</h3>
					<Table tableRows={currentRowsClimate} loading={this.state.loading} tableHeader={tableHeaderClimate} history={this.props.history} path={'/climate-city'} modelType={'climate'} doSort={false} searchWords={this.props.location.state.searchValues.split(" ")} />
                    <ReactPaginate
							previousLabel={'Previous'}
							nextLabel={'Next'}
							breakLabel={'...'}
							pageCount={totalPagesClimate}
							marginPagesDisplayed={2}
							pageRangeDisplayed={3}
							onPageChange={this.changePageClimate}
							forcePage={this.state.currentPageClimate-1}
							breakClassName={'page-item'}
							breakLinkClassName={'page-link'}
							containerClassName={'pagination'}
							pageClassName={'page-item'}
							pageLinkClassName={'page-link'}
							previousClassName={'page-item'}
							previousLinkClassName={'page-link'}
							nextClassName={'page-item'}
							nextLinkClassName={'page-link'}
							activeClassName={'active'}
						/>
                </div>
                <div className="row mt-5">
                    <h3 className="text-center mb-3">Crime</h3>
                    <Table tableRows={currentRowsCrime} loading={this.state.loading} tableHeader={tableHeaderCrime} history={this.props.history} path={'/crime-city'} modelType={'crime'} doSort={false} searchWords={this.props.location.state.searchValues.split(" ")} />
                    <ReactPaginate
							previousLabel={'Previous'}
							nextLabel={'Next'}
							breakLabel={'...'}
							pageCount={totalPagesCrime}
							marginPagesDisplayed={2}
							pageRangeDisplayed={3}
							onPageChange={this.changePageCrime}
							forcePage={this.state.currentPageCrime-1}
							breakClassName={'page-item'}
							breakLinkClassName={'page-link'}
							containerClassName={'pagination'}
							pageClassName={'page-item'}
							pageLinkClassName={'page-link'}
							previousClassName={'page-item'}
							previousLinkClassName={'page-link'}
							nextClassName={'page-item'}
							nextLinkClassName={'page-link'}
							activeClassName={'active'}
						/>
                </div>
            </div>
        )
    }
}

export default SearchPage