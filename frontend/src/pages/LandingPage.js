import './LandingPage.css'
import React, {Component} from 'react';
import brooklynImg from '../img/brooklyn.jpg';
import Background from './Background.js';

export default class LandingPage extends Component {
	render() {
		return (
			<div>
				<h1 style={{fontSize:'60px',fontFamily: 'Varela',top: '180px', left: '7%',position: 'absolute', zIndex: 1}}>Welcome to MoveCity!</h1>
				<p1 class="text-center" style={{fontSize:'20px',width: "480px",fontFamily: 'Varela',top: '260px', left: '10%',position: 'absolute', zIndex: 1}}>
				Providing relocation information for individuals who are looking to move cities in the United States. Data has been collected on crime, climate, and quality of life to show a holistic view of each city.
				</p1>
				<div class="landing">
					<Background />
				</div>
					<div className="container" style={{position: 'absolute', top: '450px',width: '47%'}}>
						<h2  style={{left:'30%',fontFamily: 'Varela'}} className="text-center" >Category Definitions</h2>
						<div className="row" style={{paddingLeft: '20%'}}>
							<div className="col-sm-3">
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Assault#Aggravated_assault">Aggravated Assault</a></p>
								<p className="text-center"><a  href="https://en.wikipedia.org/wiki/Arson">Arson</a></p>
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Burglary">Burglary</a></p>
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Homicide">Homicide</a></p>
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Larceny">Larceny</a></p>
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Property_crime">Property Crime</a></p>
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Motor_vehicle_theft">Vehicle Theft</a></p>
							</div>
							<div className="col-sm-3">
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Robbery">Robbery</a></p>
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Violent_crime">Violent Crime</a></p>
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Precipitation">Precipitation</a></p>
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Rain">Rain Days</a></p>
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/List_of_cities_by_average_temperature">Avg Tem</a></p>
								<p className="text-center"><a href="http://www.theweatherprediction.com/habyhints/227/">Annual High Temp</a></p>
								<p className="text-center"><a href="http://www.theweatherprediction.com/habyhints/227/">Annual Low Temp</a></p>
							</div>
							<div className="col-sm-3">
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Housing">Housing</a></p>
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Cost_of_living">Cost of Living</a></p>
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Commuting">Commute</a></p>
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Health_care">Healthcare</a></p>
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Education">Education</a></p>
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Environmental_quality">Environmental Quality</a></p>
								<p className="text-center"><a href="https://en.wikipedia.org/wiki/Tax">Tax</a></p>
							</div>
						</div>
					</div>
			</div>
		)
	}
}
