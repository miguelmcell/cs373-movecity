import React, {Component} from 'react'
import './Background.css'
//import BIRDS from './vanta.birds.min.js'

export default class Background extends Component {
	constructor() {
    super()
    this.vantaRef = React.createRef()
  }
  componentDidMount() {
    this.vantaEffect = window.VANTA.GLOBE({
      el: this.vantaRef.current,
      color: 0xeb29,
      color2: 0xc82929,
      size: 1.25,
      backgroundColor: '#191A1C'
    })
  }
  componentWillUnmount() {
    if (this.vantaEffect) {this.vantaEffect.destroy()}
  }
  render() {
    return (
      <div style={{}}>
        <div ref={this.vantaRef}>
        </div>
      </div>
    )
  }
}
