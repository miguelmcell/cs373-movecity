import React, {Component} from 'react'
import climateImg from '../img/illinois-heatmap.jpg'

export default class ClimateChicago extends Component {

	render() {
		return (
			<div className="text-center">
				<br /><br />
				<h1 className="text-center">Chicago Climate</h1>
				<br /><br />
                <img src={climateImg} width="40%" height="500px"/>
				<h3>Precipitation: 36 inches</h3>
				<h3>Snow: 35 inches</h3>
				<h3>Average Temperature: 51.3°F</h3>
				<h3>Annual High Temperature: 59.3°F</h3>
				<h3>Annual Low Temperature: 43.3°F</h3>
				<h3>Chicago Crime data: <a href="/crime-chicago">click here</a></h3>
				<h3>Chicago Quality of Life data: <a href="/quality-of-life-chicago">click here</a></h3>
            </div>
		)
	}
}
