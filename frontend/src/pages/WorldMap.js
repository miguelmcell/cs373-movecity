import React, { useState, useEffect } from "react"
import { geoEqualEarth, geoPath } from "d3-geo"
import { feature } from "topojson-client"

let animals =
[
  {
    "color": "#000000",
    "coordinates": [
      139.25059930824997,
      -45.16040069175003
    ],
    "name": "Yellowstriped Leatherjacket",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      140.78371741717382,
      -43.62728258282617
    ],
    "name": "Spinetail Devil Ray",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      21.985239107363405,
      53.82553910736341
    ],
    "name": "Pallas's Sandgrouse",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      23.337632412042417,
      55.17793241204242
    ],
    "name": "Shrubby Birch",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      45.29985852643888,
      35.790058526438884
    ],
    "name": "Black-headed Gull",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      49.48498874054085,
      39.97518874054085
    ],
    "name": "Asiatic Wild Ass",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      11.128105714221892,
      57.597595714221896
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      1.3780828161669172,
      47.84757281616692
    ],
    "name": "Lake Limpet",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      88.65828768737447,
      21.952487687374475
    ],
    "name": "Booted Eagle",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      96.74265758608267,
      30.036857586082675
    ],
    "name": "Obscure Flathead",
    "status": "unknown"
  },
  {
    "color": "#7aff5c",
    "coordinates": [
      73.5328765356244,
      38.87957653562438
    ],
    "name": "Common Myna",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      74.64805325301887,
      39.99475325301886
    ],
    "name": "Roach",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      28.221638172178636,
      49.736138172178634
    ],
    "name": "White-backed Woodpecker",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      20.11936624263844,
      41.63386624263843
    ],
    "name": "Hairy Lupin",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      6.65966920268797,
      47.645369202687974
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      -8.351882146672098,
      32.6338178533279
    ],
    "name": "Field Maple",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      46.75396335769808,
      42.40456335769808
    ],
    "name": "Greater Spotted Eagle",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      53.41194703724482,
      49.06254703724482
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      -163.13736081410454,
      -6.730960814104543
    ],
    "name": "Large-headed Scorpionfish",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      -165.61953783264377,
      -9.21313783264378
    ],
    "name": "Marr's Fusilier",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      130.0071743522425,
      43.28507435224249
    ],
    "name": "Grey-backed Thrush",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      120.12301197219263,
      33.40091197219262
    ],
    "name": "Eurasian Water Shrew",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      -79.37964903816068,
      36.541850961839316
    ],
    "name": "North Pacific Spiny Dogfish",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      -75.1730463962793,
      40.748453603720684
    ],
    "name": "Yellow Garden Eel",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      145.47204439190645,
      14.900544391906461
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      154.69293601470287,
      24.12143601470287
    ],
    "name": "Pelagic Porcupinefish",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      110.64152970965705,
      12.081859709657051
    ],
    "name": "Inamori's Paphiopedilum",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      107.84384541160905,
      9.284175411609056
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      163.67523902444344,
      -25.056460975556547
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      156.68458607899998,
      -32.047113921000026
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      79.31629566075746,
      21.69449566075745
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      79.04156193981295,
      21.419761939812947
    ],
    "name": "Tiger",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      175.41203523107686,
      7.943468231076859
    ],
    "name": "Black-banded Snapper",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      165.45146583077653,
      -2.017101169223472
    ],
    "name": "Diaphanous Hatchet Fish",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      27.668744877456042,
      62.34934487745604
    ],
    "name": "Herb Barbaras",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      18.4782117966879,
      53.1588117966879
    ],
    "name": "Corncrake",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      31.1274126010956,
      66.3357126010956
    ],
    "name": "Common Club-rush",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      21.6816149306586,
      56.8899149306586
    ],
    "name": "Black Tern",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      187.0545726629353,
      -9.459327337064712
    ],
    "name": "Thompson's Surgeonfish",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      169.7011245315499,
      -26.81277546845009
    ],
    "name": "Lined Surgeonfish",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      -1.726727838178944,
      44.77890216182106
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      0.5724258101512234,
      47.07805581015123
    ],
    "name": "Narrow-leaved Water-plantain",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      1.8230610851116955,
      70.6274710851117
    ],
    "name": "Ivory Gull",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      -2.3896138186571143,
      66.4147961813429
    ],
    "name": "Black-bellied Angler",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      82.87591057549709,
      9.944590575497092
    ],
    "name": "Spineless Forest Lizard",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      89.14277669781147,
      16.211456697811467
    ],
    "name": "Angled Pierrot",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      79.02941825084214,
      9.69271825084213
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      63.80546900094109,
      -5.531230999058919
    ],
    "name": "Bolin's Lanternfish",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      169.28695020617914,
      5.2624102061791636
    ],
    "name": "Honeycomb Grouper",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      175.3567628063556,
      11.332222806355604
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      96.63261323119528,
      22.590413231195285
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      101.00628861608057,
      26.964088616080566
    ],
    "name": "Buro Moray",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      116.41177684193454,
      57.38767684193455
    ],
    "name": "Alpine Pika",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      111.58926429080041,
      52.56516429080041
    ],
    "name": "Eastern Imperial Eagle",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      27.23179890737123,
      29.025798907371225
    ],
    "name": "Gray Triggerfish",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      35.03714757058177,
      36.83114757058177
    ],
    "name": "Brown Vetchling",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      15.52762038339306,
      51.19492038339306
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      21.7083744304704,
      57.375674430470404
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      19.676531896185427,
      58.788531896185425
    ],
    "name": "Wood Warbler",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      11.747184067121918,
      50.859184067121916
    ],
    "name": "Mudwort",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      22.15769544540435,
      65.26589544540435
    ],
    "name": "Blunt-flowered Rush ",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      22.09845454269614,
      65.20665454269614
    ],
    "name": "Isabelline Wheatear",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      -0.28149125288616483,
      43.83847874711383
    ],
    "name": "Laserpicio de Sierra Nevada",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      -9.757209382449094,
      34.362760617550904
    ],
    "name": "Green-Flowered Helleborne",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      138.468632199855,
      7.118932199854971
    ],
    "name": "Hairfin Eviota",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      145.03530100410444,
      13.68560100410442
    ],
    "name": "Spotbreast Angelfish",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      17.16650776348702,
      47.01200776348702
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      20.96119813324513,
      50.80669813324513
    ],
    "name": "Stinking Bean Trefoil",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      98.1259144581779,
      -14.901605541822097
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      110.51794790997407,
      -2.5095720900259257
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      121.52709284635883,
      32.10239284635883
    ],
    "name": "Hound Needlefish",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      125.17784573671636,
      35.75314573671636
    ],
    "name": "Sparse-flowered sedge",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      93.80243227464612,
      10.20803227464612
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      103.66319628509495,
      20.068796285094955
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      165.0024626976799,
      13.735172697679905
    ],
    "name": "Whitaker's Sole",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      148.90326586981578,
      -2.3640241301842266
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      9.699927635366981,
      61.32636363536698
    ],
    "name": "Whitebeam",
    "status": "unknown"
  },
  {
    "color": "#7aff5c",
    "coordinates": [
      8.706688160890282,
      60.33312416089028
    ],
    "name": "Dainty Bluet",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      34.98960234848863,
      31.906602348488637
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      43.020726777020435,
      39.93772677702044
    ],
    "name": "Great Willowherb",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      27.89023314434116,
      42.15283314434116
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      15.399979227067645,
      29.662579227067646
    ],
    "name": "Red Clover",
    "status": "unknown"
  },
  {
    "color": "#fff821",
    "coordinates": [
      -43.64621344620772,
      72.25878655379228
    ],
    "name": "Few-flowered Spike-rush",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      -58.028986208801435,
      57.87601379119857
    ],
    "name": "Sanderling",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      174.50270484102597,
      -41.55979515897403
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      168.14966104945364,
      -47.91283895054637
    ],
    "name": "Heart-leaved Kohuhu",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      63.93460244891837,
      21.651302448918376
    ],
    "name": "White-faced Storm-petrel",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      65.63410542095937,
      23.35080542095938
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      122.65718641205652,
      16.17368641205652
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      113.59352933785952,
      7.110029337859522
    ],
    "name": "Unkown",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      141.44676312712272,
      14.30970312712271
    ],
    "name": "Short-finned Pilot Whale",
    "status": "unknown"
  },
  {
    "color": "#000000",
    "coordinates": [
      131.22192396029544,
      4.084863960295435
    ],
    "name": "Ornate Goby",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      150.69717171063547,
      -5.970398289364523
    ],
    "name": "Tawny Nurse Shark",
    "status": "unknown"
  },
  {
    "color": "#ff0000",
    "coordinates": [
      138.3587067328627,
      -18.30886326713729
    ],
    "name": "Rainford's Butterflyfish",
    "status": "unknown"
  }
]

const projection = geoEqualEarth()
  .scale(250)
  .translate([ 800 / 2, 750 / 2 ])

const WorldMap = () => {
  const [geographies, setGeographies] = useState([]);
  useEffect(() => {
    fetch("/world-110m.json")
      .then(response => {
        if (response.status !== 200) {
          console.log(`There was a problem: ${response.status}`)
          return
        }
        response.json().then(worlddata => {
          setGeographies(feature(worlddata, worlddata.objects.countries).features)
        })
      })
    }, []);
    const [text, setText] = useState("Species Name:");
    useEffect(() => {
      console.log(text);
    });
  // const [animals,setAnimals] = useState([]);
  // useEffect(() => {
  //   let curPage = 1;
  //   let maxPage = 10
  //   for (var i = 1;i<=10;i++){
  //     fetch('https://endangeredanimals.me/api/animal?page='+i)
  //     .then(response => response.json())
  //     .then(data => {
  //       data['objects'].forEach(object => {
  //         let builtObject = {};
  //         // console.log(object['name']);
  //         builtObject['name'] = object['name'];
  //         // console.log(object['ecologies'][0]['latitude'] + ', ' + object['ecologies'][0]['longitude']);
  //         let latLong = [object['ecologies'][0]['latitude'],object['ecologies'][0]['longitude']]
  //         builtObject['coordinates'] = latLong;
  //         let pop = object['population'];
  //         // console.log(pop);
  //         let color = '#000000';
  //         if (pop === 'Increasing'){
  //           color = '#7aff5c';
  //         } else if (pop === 'Decreasing'){
  //           color = '#ff0000';
  //         } else if (pop === 'Stable'){
  //           color = '#fff821';
  //         }
  //         // console.log(color);
  //         builtObject['color'] = color;
  //         builtObject['status'] = 'unknown';
  //         // console.log(builtObject);
  //         animals.push(builtObject);
  //         setAnimals(animals)
  //       });
  //       console.log(animals)
  //       // console.log(data['objects'])
  //     });
  //
  //   }
  //
    // }, [])
  const handleCountryClick = countryIndex => {
    console.log("Clicked on country: ", geographies[countryIndex])
  }

  const handleMarkerClick = i => {
    console.log("Marker: ", animals[i])
    if(animals[i].name == null){
        setText("Species Name: Unkown");
    }
    else{
      setText("Species Name: " + animals[i].name);
    }
  }
  {/* Status: (Increasing,GREEN),(Unknown,BLACK),(STABLE,YELLOW),(Decreasing,RED) */}

  return (
    <div>
      <p1 style={{paddingLeft: '10px'}}>Legend:</p1>
      <br/>
      <p1 style={{color: 'green',paddingLeft: '10px'}}>Green </p1>: Increasing Population Rate
      <br/>
      <p1 style={{color: 'yellow',paddingLeft: '10px'}}>Yellow: </p1> Stable Population Rate
      <br/>
      <p1 style={{color: 'red',paddingLeft: '10px'}}>Red: </p1> Decreasing Population Rate
      <br/>
      <p1 style={{color: 'white',paddingLeft: '10px'}}>Black: </p1> Unkown Population Rate
      <br/>
      <h3 style={{position: 'absolute',paddingLeft: '350px'}} >
      {text}
      </h3>
      <br/>
      <svg width={ '100%' } height={ 800 } viewBox="0 0 1000 1000">
        <g className="countries">
          {
            geographies.map((d,i) => (
              <path
                key={ `path-${ i }` }
                d={ geoPath().projection(projection)(d) }
                className="country"
                fill={ `rgba(255,255,255,0.2)` }
                stroke="#FFFFFF"
                strokeWidth={ 0.5 }
                // onClick={ () => handleCountryClick(i) }
              />
            ))
          }
        </g>
        <g className="markers">
          {
            animals.map((animal, i) => (
              <circle
                key={ `marker-${i}` }
                cx={ projection(animal.coordinates)[0] }
                cy={ projection(animal.coordinates)[1] }
                r={5}
                fill={animal.color}
                stroke="#FFFFFF"
                className="marker"
                onMouseOver={ () => handleMarkerClick(i) }
              />
            ))
          }
        </g>
      </svg>
    </div>
  )
}

export default WorldMap
