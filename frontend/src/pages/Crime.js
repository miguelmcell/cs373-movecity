import React, {Component} from 'react'
import axios from 'axios'
import './Crime.css'
import Table from '../components/Table'
import ReactPaginate from 'react-paginate'

// Table header
const tableHeader = ["City", "Aggravated Assault", "Arson", "Burglary", "Homicide", "Larceny", "Property Crime", "Vehicle Theft", "Robbery", "Violent Crime"]

class Crime extends Component {
	constructor() {
        super()
        this.state = {
			tableRows: [],
			loading: true,
			currentPage: 1,
            tableRowsPerPage: 10,
			sortDirection: '',
			sortColumn: '',

			// filter values
			filter: '',
			aggravatedAssaultValue: '',
			aggravatedAssaultOp: '',
			arsonValue: '',
			arsonOp: '',
			burglaryValue: '',
			burglaryOp: '',
			homicideValue: '',
			homicideOp: '',
			larcenyValue: '',
			larcenyOp: '',
			propertyCrimeValue: '',
			propertyCrimeOp: '',
			vehicleTheftValue: '',
			vehicleTheftOp: '',
			robberyValue: '',
			robberyOp: '',
			violentCrimeValue: '',
			violentCrimeOp: '',

			// search values
			searchValues: '',
			search: '',
		}
		this.sortData = this.sortData.bind(this)
		this.handleFilterChange = this.handleFilterChange.bind(this)
		this.handleFilterSubmit = this.handleFilterSubmit.bind(this)
		this.handleFilterReset = this.handleFilterReset.bind(this)
		this.handleSearchSubmit = this.handleSearchSubmit.bind(this)
		this.handleSearchChange = this.handleSearchChange.bind(this)
	}

    async componentDidMount() {
		this.loadData()
	}

	async loadData() {
		try {
			let filters = ''
			if(this.state.filter !== '') {
				filters = `?q={"filters":${this.state.filter}}`
			}
			const result = await axios.get(`/v1/api/Crime${filters}`)
			this.setState({
				tableRows: result.data.objects,
				loading: false
			});
		} catch (error) {
			this.setState({
				error,
				loading: false
			});
		}
	}
	
	changePage = data  => {
        this.setState({
            currentPage: data.selected + 1
        })
    }

	sortData(sortDirection, sortColumn) {
		const columns = {"City": "Crime_idCity", "Aggravated Assault": "Aggravated_Assault", "Arson": "Arson", "Burglary": "Burglary", "Homicide": "Homicide", "Larceny": "Larceny", "Property Crime": "Property_Crime", "Vehicle Theft": "Vehicle_Theft", "Robbery": "Robbery", "Violent Crime": "Violent_Crime"}
		let sortedData = []
		if(sortDirection === 'asc') {
			sortedData = this.state.tableRows.sort(function(a, b){return a[columns[sortColumn]]-b[columns[sortColumn]]})
		} else {
			sortedData = this.state.tableRows.sort(function(a, b){return b[columns[sortColumn]]-a[columns[sortColumn]]})
		}

		this.setState({
			tableRows: sortedData,
			currentPage: 1
		})
	}

	checkNaNFilters(...args) {
		args.forEach(arg => {
			if(isNaN(Number(arg))) {
				alert(`"${arg}" is not a number`)
				return false
			}
		})
		return true
	}

	handleFilterSubmit(event) {
		if(this.checkNaNFilters(this.state.aggravatedAssaultValue, this.state.arsonValue, this.state.burglaryValue, this.state.homicideValue, this.state.larcenyValue, this.state.propertyCrimeValue, this.state.vehicleTheftValue, this.state.robberyValue, this.state.violentCrimeValue)) {
			let filter = [{"and": []}]
			if(this.state.aggravatedAssaultOp !== '' && this.state.aggravatedAssaultValue !== '') {
				const op = this.state.aggravatedAssaultOp === "Greater Than" ? "gt" : this.state.aggravatedAssaultOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Aggravated_Assault", "op": op, "val": Number(this.state.aggravatedAssaultValue)})
			}
			if(this.state.arsonOp !== '' && this.state.arsonValue !== '') {
				const op = this.state.arsonOp === "Greater Than" ? "gt" : this.state.arsonOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Arson", "op": op, "val": Number(this.state.arsonValue)})
			}
			if(this.state.burglaryOp !== '' && this.state.burglaryValue !== '') {
				const op = this.state.burglaryOp === "Greater Than" ? "gt" : this.state.burglaryOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Burglary", "op": op, "val": Number(this.state.burglaryValue)})
			}
			if(this.state.homicideOp !== '' && this.state.homicideValue !== '') {
				const op = this.state.homicideOp === "Greater Than" ? "gt" : this.state.homicideOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Homicide", "op": op, "val": Number(this.state.homicideValue)})
			}
			if(this.state.larcenyOp !== '' && this.state.larcenyValue !== '') {
				const op = this.state.larcenyOp === "Greater Than" ? "gt" : this.state.larcenyOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Larceny", "op": op, "val": Number(this.state.larcenyValue)})
			}
			if(this.state.propertyCrimeOp !== '' && this.state.propertyCrimeValue !== '') {
				const op = this.state.propertyCrimeOp === "Greater Than" ? "gt" : this.state.propertyCrimeOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Property_Crime", "op": op, "val": Number(this.state.propertyCrimeValue)})
			}
			if(this.state.vehicleTheftOp !== '' && this.state.vehicleTheftValue !== '') {
				const op = this.state.vehicleTheftOp === "Greater Than" ? "gt" : this.state.vehicleTheftOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Vehicle_Theft", "op": op, "val": Number(this.state.vehicleTheftValue)})
			}
			if(this.state.robberyOp !== '' && this.state.robberyValue !== '') {
				const op = this.state.robberyOp === "Greater Than" ? "gt" : this.state.robberyOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Robbery", "op": op, "val": Number(this.state.robberyValue)})
			}
			if(this.state.violentCrimeOp !== '' && this.state.violentCrimeValue !== '') {
				const op = this.state.violentCrimeOp === "Greater Than" ? "gt" : this.state.violentCrimeOp === "Less Than" ? "lt" : "eq"
				filter[0].and.push({"name": "Violent_Crime", "op": op, "val": Number(this.state.violentCrimeValue)})
			}

			this.setState({
				filter: JSON.stringify(filter),
				loading: true
			},() => this.loadData())
		}

		event.preventDefault()
	}

	handleFilterReset(event) {
		this.setState({
			filter: '',
			aggravatedAssaultValue: '',
			aggravatedAssaultOp: '',
			arsonValue: '',
			arsonOp: '',
			burglaryValue: '',
			burglaryOp: '',
			homicideValue: '',
			homicideOp: '',
			larcenyValue: '',
			larcenyOp: '',
			propertyCrimeValue: '',
			propertyCrimeOp: '',
			vehicleTheftValue: '',
			vehicleTheftOp: '',
			robberyValue: '',
			robberyOp: '',
			violentCrimeValue: '',
			violentCrimeOp: '',
			search: '',
			searchValues: '',
		}, this.loadData)
		alert("You have reset the table")
	}

	handleFilterChange(event) {
		const target = event.target;
		const name = target.name;
		const value = target.value

		this.setState({
			[name]: value
		});
	}

	handleSearchChange(event) {
		this.setState({
			searchValues: event.target.value
		})
	}

	handleSearchSubmit() {
		const words = this.state.searchValues.split(" ")
		const columns = ["Crime_idCity", "Aggravated_Assault", "Arson", "Burglary", "Homicide", "Larceny", "Property_Crime", "Vehicle_Theft", "Robbery", "Violent_Crime"]
		const searchData = this.state.tableRows.filter(row => {
			for(let i = 0; i < words.length; i++) {
				for(let j = 0; j < columns.length; j++) {
					if(columns[j] === "Crime_idCity" && isNaN(Number(words[i]))) {
						if(row["CityName"].toLowerCase().includes(words[i].toLowerCase())) {
							return true
						} else {
							break
						}
					} else if(!isNaN(Number(words[i]))){
						if(row[`${columns[j]}`] === Number(words[i])) {
							return true
						}
					}
				}
			}
			return false
		})

		this.setState({
			tableRows: searchData
		})

	}

	render() {
		if(this.state.loading) {
            return <h2>Loading...</h2>
        }
		
		// Get current rows
        const indexOfLastRow = this.state.currentPage * this.state.tableRowsPerPage
        const indexOfFirstRow = indexOfLastRow - this.state.tableRowsPerPage
        const currentRows = this.state.tableRows.slice(indexOfFirstRow, indexOfLastRow)
        const totalPages = Math.ceil(this.state.tableRows.length / this.state.tableRowsPerPage)

		return (
			
			<div className="crime">
				<br /><br />
				<h1 className="text-center">Crime</h1>
				<br /><br />
				<div className="crime-content">
					<div className="filter mb-3">
					<div className="form-group row mx-1">
							<button className="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
								Filter Search Results
							</button>
							<div className="col-xs-4 ml-5 mr-1">
								<input className="form-control" type="text" placeholder="Search" aria-label="Search" onChange={this.handleSearchChange}/>
								{/* <button className="btn aqua-gradient btn-rounded btn-sm my-0" type="submit">Search</button> */}
							</div>
							<div className="col-xs-4 mr-1">
								{/* <input className="form-control" type="text" placeholder="Search" aria-label="Search" /> */}
								<button className="btn btn-primary btn-rounded btn" type="submit" onClick={this.handleSearchSubmit}>Search</button>
							</div>
							<div className="col-xs-4">
								<button className="btn btn-primary btn-rounded btn" type="reset" onClick={this.handleFilterReset}>Reset</button>
							</div>
						</div>
						<div className="collapse" id="collapseFilter">
							<div className="card card-body bg-dark">
								<form className="container" onSubmit={this.handleFilterSubmit} onReset={this.handleFilterReset} >
									<label className="row">
										Aggravated Assault:
										<select className="mx-2" name="aggravatedAssaultOp" value={this.state.aggravatedAssaultOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="aggravatedAssaultValue" value={this.state.aggravatedAssaultValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Arson:
										<select className="mx-2" name="arsonOp" value={this.state.arsonOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="arsonValue" value={this.state.arsonValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Burglary:
										<select className="mx-2" name="burglaryOp" value={this.state.burglaryOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="burglaryValue" value={this.state.burglaryValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Homicide:
										<select className="mx-2" name="homicideOp" value={this.state.homicideOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="homicideValue" value={this.state.homicideValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Larceny:
										<select className="mx-2" name="larcenyOp" value={this.state.larcenyOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="larcenyValue" value={this.state.larcenyValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Property Crime:
										<select className="mx-2" name="propertyCrimeOp" value={this.state.propertyCrimeOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="propertyCrimeValue" value={this.state.propertyCrimeValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Vehicle Theft:
										<select className="mx-2" name="vehicleTheftOp" value={this.state.vehicleTheftOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="vehicleTheftValue" value={this.state.vehicleTheftValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Robbery:
										<select className="mx-2" name="robberyOp" value={this.state.robberyOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="robberyValue" value={this.state.robberyValue} onChange={this.handleFilterChange} />
									</label>
									<label className="row">
										Violent Crime:
										<select className="mx-2" name="taxOp" value={this.state.violentCrimeOp} onChange={this.handleFilterChange}>
											<option value="">Choose Operator...</option>
											<option value="Greater Than">Greater Than</option>
											<option value="Less Than">Less Than</option>
											<option value="Equals">Equals</option>
										</select>
										<input type="text" placeholder="Enter a number..." name="violentCrimeValue" value={this.state.violentCrimeValue} onChange={this.handleFilterChange} />
									</label>

									<input className="mx-2 my-2" type="submit" value="Submit" />
									<input type="reset" value="Reset" />
								</form>
							</div>
						</div>
					</div>
					<Table tableRows={currentRows} loading={this.state.loading} tableHeader={tableHeader} history={this.props.history} path={'/crime-city'} modelType={'crime'} doSort={true} sortData={this.sortData.bind(this)} searchWords={this.state.searchValues.split(" ")} />
					<div className="page-num">
						<ReactPaginate
							previousLabel={'Previous'}
							nextLabel={'Next'}
							breakLabel={'...'}
							pageCount={totalPages}
							marginPagesDisplayed={2}
							pageRangeDisplayed={3}
							onPageChange={this.changePage}
							forcePage={this.state.currentPage-1}
							breakClassName={'page-item'}
							breakLinkClassName={'page-link'}
							containerClassName={'pagination'}
							pageClassName={'page-item'}
							pageLinkClassName={'page-link'}
							previousClassName={'page-item'}
							previousLinkClassName={'page-link'}
							nextClassName={'page-item'}
							nextLinkClassName={'page-link'}
							activeClassName={'active'}
						/>

					</div>
				</div>
			</div>
		)
	}
}

export default Crime
