# For prod
# nginx, checkout dev, kubectl cluster

set -e
# begin testing mocha
cd frontend
sudo chown -R 1000:1000 "/home/ubuntu/.npm"
npm install
cd ../testing/reactUnitTesting/tests/
npm install
# npm install axios
./startTest.sh
cd ../../../
# begin testing selenium
# --------------
# cd testing/selenium/tests/
# ./runTests.sh
# -----------------

# start deploying
cd frontend
npm run-script build
sudo docker build --tag miguelmcell/movecity:dev .
sudo docker push miguelmcell/movecity:dev
/usr/local/bin/kubectl delete deployment movecity-deployment
/usr/local/bin/kubectl apply -f movecity-deployment.yaml
cd ..

# begin testing python backend
cd testing/pythonUnitTesting
pip3 install -r requirements.txt
sleep 10
python3 main.py

cd ../postman
sudo npm install -g newman
newman run postman_tests.postman_collection.json
