Miguel Mendoza, mam23575, @miguelmcell

Adhithya Balaji, acb3985, @adhithya-b

Justin Zhang, jmz655, @justzhang

Mehrdad Darraji, md38832, @mehrdad14

Gage Courtaway, gtc375, @gcourtaway

Final SHA:
a3f404d799b5b274af4fb2611212922f8b063e28

Project Leader: Miguel Mendoza

https://gitlab.com/miguelmcell/cs373-movecity/pipelines

https://movecity.live

Estimated Completion Time:  
Miguel Mendoza: 11 hours  
Adhithya Balaji: 10 hours  
Justin Zhang: 11 hours  
Mehrdad Darraji: 24 hours  
Gage Courtaway:18 hours  

Actual Completion Time:  
Miguel Mendoza: 28 hours  
Adhithya Balaji: 15 hours  
Justin Zhang: 15 hours  
Mehrdad Darraji: 34 hours  
Gage Courtaway: 22 hours  

Comments:
