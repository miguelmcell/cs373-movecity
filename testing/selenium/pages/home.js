// in e2e/pages/home.js
const webdriver = require('selenium-webdriver');
const By = webdriver.By;
const until = webdriver.until;

module.exports = function(driver) {
    const elements = {
        homeTab: By.xpath("//*[@id='navbarText']/ul/li[1]/a"),
        qolTab: By.xpath("//*[@id='navbarText']/ul/li[2]/a"),
        climateTab: By.xpath("//*[@id='navbarText']/ul/li[3]/a"),
        crimeTab: By.xpath("//*[@id='navbarText']/ul/li[4]/a"),
        aboutTab: By.xpath("//*[@id='navbarText']/ul/li[6]/a"),
        qualityText: By.xpath("//*[@id='root']/div/div[1]/div[2]/div/div/h1"),
        welcomeText: By.xpath("//*[text()='Welcome to MoveCity!']"),
        climateText: By.xpath("//*[@id='root']/div/div[1]/div[2]/div/div/h1"),
        crimeText: By.xpath("//*[@id='root']/div/div[1]/div[2]/div/div/h1"),
        aboutText: By.xpath("//*[@id='root']/div/div[1]/div[2]/div/div/h1[1]"),
        gitlabLink: By.xpath("//*[@id='root']/div/div[1]/div[2]/div/div/h3/a"),
        miguelName:By.xpath("//*[@id='root']/div/div[1]/div[2]/div/div/div[1]/div/div[4]/div/h3/text()"),
        justinName: By.xpath("//*[@id='root']/div/div[1]/div[2]/div/div/div[1]/div/div[6]/div/h3/text()"),
        gageName: By.xpath("//*[@id='root']/div/div[1]/div[2]/div/div/div[1]/div/div[2]/div/h3/text()"),
        mehrdadName:By.xpath("//*[@id='root']/div/div[1]/div[2]/div/div/div[1]/div/div[3]/div/h3/text()"),
        adhithyaName:By.xpath("//*[@id='root']/div/div[1]/div[2]/div/div/div[1]/div/div[1]/div/h3/text()"),
        // nameInput: By.css('.autocomplete'),
        // nameSuggestion: By.css('.suggestion'),
        // submitButton: By.css('.submit'),
    };
    return {
        url:  'http://dev.movecity.live',
        elements: elements,
        waitUntilVisible1: function() {
            return driver.wait(until.elementLocated(elements.welcomeText));
        },
        waitUntilVisible2: function() {
            return driver.wait(until.elementLocated(elements.qualityText));
        },
        waitUntilVisible3: function() {
            return driver.wait(until.elementLocated(elements.climateText));
        },
        waitUntilVisible4: function() {
            return driver.wait(until.elementLocated(elements.crimeText));
        },
        waitUntilVisible5: function() {
            return driver.wait(until.elementLocated(elements.aboutText));
        },
        navigate: function() {
            driver.navigate().to(this.url);
            return this.waitUntilVisible1();
        },
        navigateToQol: function() {
            driver.findElement(elements.qolTab).click();
            return this.waitUntilVisible2();
        },
        navigateToClimate: function() {
            driver.findElement(elements.climateTab).click();
            return this.waitUntilVisible3();
        },
        navigateToCrime: function() {
            driver.findElement(elements.crimeTab).click();
            return this.waitUntilVisible4();
        },
        navigateToAbout: function() {
            driver.findElement(elements.aboutTab).click();
            return this.waitUntilVisible5();
        },
        getSplashPage: async function() {
            return driver.findElement(elements.welcomeText).getText();
        },
        getQolText: async function() {
            return driver.findElement(elements.qualityText).getText();
        },
        getClimateText: async function() {
          return driver.findElement(elements.climateText).getText();
        },
        getCrimeText: async function() {
          return driver.findElement(elements.crimeText).getText();
        },
        getAboutText: async function() {
          return driver.findElement(elements.aboutText).getText();
        },
        getGitlabLink: async function() {
          return driver.findElement(elements.gitlabLink).getText();
        },
        getNameMiguel: async function() {
          return driver.findElement(elements.miguelName).getText();
        },
        getNameJustin: async function() {
          return driver.findElement(elements.justinName).getText();
        },
        getNameGage: async function() {
          return driver.findElement(elements.gageName).getText();
        },
        getNameMehrdad: async function() {
          return driver.findElement(elements.mehrdadName).getText();
        },
        getNameAdhithya: async function() {
          return driver.findElement(elements.adhithyaName).getText();
        },
        // enterName: function(value) {
        //     driver.findElement(elements.nameInput).sendKeys(value);
        //     driver.wait(until.elementLocated(elements.nameSuggestion));
        //     driver.findElement(elements.nameSuggestion).click();
        //     return driver.wait(until.elementIsNotVisible(By.css(elements.nameSuggestion)));
        // },

        // getName: function() {
        //     return driver.findElement(elements.nameInput).getAttribute('value')
        // }
        // submit: function() {
        //     return driver.findElement(elements.submitButton).click();
        // },
    };
};
