const webdriver = require('selenium-webdriver');
var assert = require('assert');
var expect = require('chai').expect

const homeToQol = new Promise((resolve) => {
  const browserName = 'firefox'; // Switch to 'firefox' if desired
  const capabilityName = 'moz:firefoxOptions'; // Switch to 'moz:firefoxOptions' if desired
  let browserOptions = {
  	'args': [
  		'--headless',
  		'--disable-gpu',
  		'--no-sandbox'
  	]
  };
  var browserCapabilities = browserName === 'chrome'
  	? webdriver.Capabilities.chrome()
  	: webdriver.Capabilities.firefox();
  browserCapabilities = browserCapabilities.set(capabilityName, browserOptions);
  let builder = new webdriver.Builder()
      .forBrowser(browserName);
  let driver = builder.withCapabilities(browserCapabilities).build();
  const homePage = require('../pages/home')(driver);

  homePage.navigate().then(function(result){
    homePage.navigateToQol().then(function(result){
      homePage.getQolText().then(function(result){
        driver.close();
        resolve(result);
      })
    });
  });
});

const homeToClimate = new Promise((resolve) => {
  const browserName = 'firefox'; // Switch to 'firefox' if desired
  const capabilityName = 'moz:firefoxOptions'; // Switch to 'moz:firefoxOptions' if desired
  let browserOptions = {
  	'args': [
  		'--headless',
  		'--disable-gpu',
  		'--no-sandbox'
  	]
  };
  var browserCapabilities = browserName === 'chrome'
  	? webdriver.Capabilities.chrome()
  	: webdriver.Capabilities.firefox();
  browserCapabilities = browserCapabilities.set(capabilityName, browserOptions);
  let builder = new webdriver.Builder()
      .forBrowser(browserName);
  let driver = builder.withCapabilities(browserCapabilities).build();
  const homePage = require('../pages/home')(driver);
  homePage.navigate().then(function(result){
    homePage.navigateToClimate().then(function(result){
      homePage.getClimateText().then(function(result){
        driver.close();
        resolve(result);
      })
    });
  });
});

const homeToCrime = new Promise((resolve) => {
  const browserName = 'firefox'; // Switch to 'firefox' if desired
  const capabilityName = 'moz:firefoxOptions'; // Switch to 'moz:firefoxOptions' if desired
  let browserOptions = {
  	'args': [
  		'--headless',
  		'--disable-gpu',
  		'--no-sandbox'
  	]
  };
  var browserCapabilities = browserName === 'chrome'
  	? webdriver.Capabilities.chrome()
  	: webdriver.Capabilities.firefox();
  browserCapabilities = browserCapabilities.set(capabilityName, browserOptions);
  let builder = new webdriver.Builder()
      .forBrowser(browserName);
  let driver = builder.withCapabilities(browserCapabilities).build();
  const homePage = require('../pages/home')(driver);
  homePage.navigate().then(function(result){
    homePage.navigateToCrime().then(function(result){
      homePage.getCrimeText().then(function(result){
        driver.close();
        resolve(result);
      })
    });
  });
});

const getGitlabLink = new Promise((resolve) => {
  const browserName = 'firefox'; // Switch to 'firefox' if desired
  const capabilityName = 'moz:firefoxOptions'; // Switch to 'moz:firefoxOptions' if desired
  let browserOptions = {
  	'args': [
  		'--headless',
  		'--disable-gpu',
  		'--no-sandbox'
  	]
  };
  var browserCapabilities = browserName === 'chrome'
  	? webdriver.Capabilities.chrome()
  	: webdriver.Capabilities.firefox();
  browserCapabilities = browserCapabilities.set(capabilityName, browserOptions);
  let builder = new webdriver.Builder()
      .forBrowser(browserName);
  let driver = builder.withCapabilities(browserCapabilities).build();
  const homePage = require('../pages/home')(driver);
    homePage.navigate().then(function(result){
      homePage.navigateToAbout().then(function(result){
        homePage.getGitlabLink().then(function(result){
          driver.close();
          resolve(result);
        })
      });
    });
});
const homeToAbout = new Promise((resolve) => {
  const browserName = 'firefox'; // Switch to 'firefox' if desired
  const capabilityName = 'moz:firefoxOptions'; // Switch to 'moz:firefoxOptions' if desired
  let browserOptions = {
  	'args': [
  		'--headless',
  		'--disable-gpu',
  		'--no-sandbox'
  	]
  };
  var browserCapabilities = browserName === 'chrome'
  	? webdriver.Capabilities.chrome()
  	: webdriver.Capabilities.firefox();
  browserCapabilities = browserCapabilities.set(capabilityName, browserOptions);
  let builder = new webdriver.Builder()
      .forBrowser(browserName);
  let driver = builder.withCapabilities(browserCapabilities).build();
  const homePage = require('../pages/home')(driver);
  homePage.navigate().then(function(result){
    homePage.navigateToAbout().then(function(result){
      homePage.getAboutText().then(function(result){
        driver.close();
        resolve(result);
      })
    });
  });
});

describe ('clicks through all tabs ensuring routes work', function() {
  it('Home to Quality of Life', async () => {
      const result = await homeToQol;
      expect(result).to.equal('Quality Of Life');
  });

  it('Home to Climate', async () => {
      const result = await homeToClimate;
      expect(result).to.equal('Climate');
  });

  it('Home to Crime', async () => {
      const result = await homeToCrime;
      expect(result).to.equal('Crime');
  });

  it('Home to About', async () => {
      const result = await homeToAbout;
      expect(result).to.equal('About Us');
  });
})
describe ('verifies the About Us page', function() {
  it('Verify gitlab link', async () => {
      const result = await getGitlabLink;
      expect(result).to.equal('GitLab Repository');
  });

  it('Verify Miguel', async () => {
      const result = await homeToClimate;
      expect(result).to.equal('Climate');
  });

  it('Verify Justin', async () => {
      const result = await homeToCrime;
      expect(result).to.equal('Crime');
  });

  it('Verify Gage', async () => {
      const result = await homeToAbout;
      expect(result).to.equal('About Us');
  });

  it('Verify Mehrdad', async () => {
      const result = await homeToAbout;
      expect(result).to.equal('About Us');
  });

  it('Verify Adhithya', async () => {
      const result = await homeToAbout;
      expect(result).to.equal('About Us');
  });
});
