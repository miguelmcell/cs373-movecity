var expect = require('chai').expect

const testBackend = new Promise((resolve) => {
  const axios = require('axios').default;
  const requests = require('../../../frontend/src/util/requests')(axios)
  requests.hitTestEndpoint(axios).then(function(result){
    resolve(result);
  });
});
const testGitlabAPI = new Promise((resolve) => {
  const axios = require('axios').default;
  const requests = require('../../../frontend/src/util/requests')(axios)
  requests.hitGitlabIssuesAPI(axios).then(function(result){
    resolve(result);
  });
});
const testGitlabCommitsAPI = new Promise((resolve) => {
  const axios = require('axios').default;
  const requests = require('../../../frontend/src/util/requests')(axios)
  requests.hitGitlabCommitsAPI(axios).then(function(result){
    resolve(result);
  });
});
const testReactSortData = new Promise((resolve) => {
  const axios = require('axios').default;
  const functions = require('../../../frontend/src/util/functions')('asc',[],axios)
  functions.sortData().then(function(result){
    resolve(result);
  });
  // resolve(sort.sortData('asc',[]));
});
const testCrimeData = new Promise((resolve) => {
  const axios = require('axios').default;
  const functions = require('../../../frontend/src/util/functions')('asc',[],axios)
  functions.checkCrime().then(function(result){
    resolve(result);
  });
  // resolve(sort.sortData('asc',[]));
});
const testCityData = new Promise((resolve) => {
  const axios = require('axios').default;
  const functions = require('../../../frontend/src/util/functions')('asc',[],axios)
  functions.checkCity().then(function(result){
    resolve(result);
  });
  // resolve(sort.sortData('asc',[]));
});
const testChangingPages = new Promise((resolve) => {
  const axios = require('axios').default;
  const functions = require('../../../frontend/src/util/functions')('asc',[],axios)
  functions.changePage().then(function(result){
    resolve(result);
  });
  // resolve(sort.sortData('asc',[]));
});
const testOnClickClimate = new Promise((resolve) => {
  const axios = require('axios').default;
  const functions = require('../../../frontend/src/util/functions')('asc',[],axios)
  functions.onClickClimate().then(function(result){
    resolve(result);
  });
  // resolve(sort.sortData('asc',[]));
});
const testPageNotFound = new Promise((resolve) => {
  const axios = require('axios').default;
  const functions = require('../../../frontend/src/util/functions')('asc',[],axios)
  functions.pageNotFound().then(function(result){
    resolve(result);
  });
  // resolve(sort.sortData('asc',[]));
});
const getCityImageLink = new Promise((resolve) => {
  const axios = require('axios').default;
  const functions = require('../../../frontend/src/util/functions')('asc',[],axios)
  functions.getCityImageLink().then(function(result){
    resolve(result);
  });
  // resolve(sort.sortData('asc',[]));
});

describe ('Testing About Page', function() {
  it('Testing Backend', async () => {
      const result = await testBackend;
      expect(result).to.equal('active');
  });
  it('Testing gitlab Issues API', async () => {
      const result = await testGitlabAPI;
      expect(result).to.be.a('array');
  });
  it('Testing gitlab commits API', async () => {
      const result = await testGitlabCommitsAPI;
      expect(result).to.be.a('array');
  });
  it('Testing react sortData function', async () => {
      const result = await testReactSortData;
      const output = [ { Commute: 2, Cost_of_Living: 4,
    Education: 11,
    Environmental_Quality: 2,
    Healthcare: 5,
    Housing: 5,
    QOL_idCity: 1816,
    Tax: 4 },
  { Commute: 7,
    Cost_of_Living: 11,
    Education: 4,
    Environmental_Quality: 3,
    Healthcare: 7,
    Housing: 8,
    QOL_idCity: 1817,
    Tax: 8 },
  { Commute: 1,
    Cost_of_Living: 8,
    Education: 1,
    Environmental_Quality: 2,
    Healthcare: 2,
    Housing: 11,
    QOL_idCity: 1818,
    Tax: 7 } ];
      expect(result).to.eql(output);
  });
  it('Testing getCrime requests through frontend', async () => {
      const result = await testCrimeData;
      expect(result).to.be.a('Object');
  });
  it('Testing getCity requests through frontend', async () => {
      const result = await testCityData;
      expect(result).to.be.a('Object');
  });
  it('Testing changing pages', async () => {
      const result = await testChangingPages;
      expect(result).to.equal(2);
  });
  it('Testing onClicking climate', async () => {
      const result = await testOnClickClimate;
      expect(result).to.be.above(1000);
  });
  it('Testing page not found', async () => {
      const result = await testPageNotFound;
      expect(result).to.be.a('string');
  });
  it('Testing cityImage api from frontend', async () => {
      const result = await getCityImageLink;
      expect(result).to.be.a('string');
  });
})
