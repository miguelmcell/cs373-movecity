from unittest import main, TestCase
import requests
import json


class TestFlaskMethods(TestCase):
    def testIfActive(self):
        url = "http://dev.movecity.live/v1/status"
        status = requests.get(url).text
        self.assertEqual(status,'active')

    def testAllCities(self):
        url = "http://dev.movecity.live/v1/api/City"
        cities = json.loads(requests.get(url).text)['num_results']
        self.assertGreater(cities,1100)

    def testAllClimates(self):
        url = "http://dev.movecity.live/v1/api/Climate"
        climates = json.loads(requests.get(url).text)['num_results']
        self.assertGreater(climates,1100)

    def testAllCrime(self):
        url = "http://dev.movecity.live/v1/api/Crime"
        crimes = json.loads(requests.get(url).text)['num_results']
        self.assertGreater(crimes,1100)

    def testCityImage(self):
        url = "http://dev.movecity.live/v1/getCityImage/Austin"
        link = requests.get(url).text
        expect = 'https://maps.googleapis.com/maps/api/place/photo?'
        self.assertEqual(expect in link,True)

    def testCrimeAggravatedAssault(self):
        url = "http://dev.movecity.live/v1/api/Crime"
        crime = json.loads(requests.get(url).text)['objects'][0]['Aggravated_Assault']
        self.assertEqual(crime is not None,True)

    def testSortingQuery(self):
        url = 'http://dev.movecity.live/v1/api/QOL?q={%22order_by%22:[{%22field%22:%22Commute%22,%22direction%22:%22asc%22}]}'
        climates = json.loads(requests.get(url).text)['objects']
        qol1 = climates[0]['Commute']
        qol2 = climates[120]['Commute']
        self.assertEqual(qol1 < qol2,True)

    def testCitySearch(self):
        url = 'http://dev.movecity.live/v1/api/City?q={%22filters%22:[{%22name%22:%20%22CityName%22,%20%22op%22:%20%22eq%22,%20%22val%22:%20%22%27Birmingham%27%22}]}'
        crime = json.loads(requests.get(url).text)['objects'][0]
        self.assertEqual(crime is not None,True)

    def testCityFiltering(self):
        url = 'http://dev.movecity.live/v1/api/QOL?q={%22filters%22:[{%22name%22:%22Commute%22,%22op%22:%22gt%22,%22val%22:5}]}'
        qol = json.loads(requests.get(url).text)['objects'][0]
        self.assertEqual(qol['Commute'] > 2,True)

    def testCityId(self):
        url = 'http://dev.movecity.live/v1/api/City'
        city = json.loads(requests.get(url).text)['objects'][0]
        self.assertEqual(str(city['idCity']).isdigit(),True)

if __name__ == "__main__":
    main()
