from flask import Blueprint
from requests import get

gitlabBP = Blueprint('gitlabBP', __name__)


@gitlabBP.route('/commits', methods = ['GET'])
def getAllCommits():
	req = get("https://gitlab.com/api/v4/projects/14523582/repository/commits/", headers = {"PRIVATE-TOKEN":"ic7tQSERwW4SnsdxXfuD"})
	print(req.headers)
	return str(req.headers.get('X-Total'))

@gitlabBP.route('/commits/<user>', methods = ['GET'])
def getUserCommits(user):
	req = get("https://gitlab.com/api/v4/projects/14523582/repository/commits/", headers = {"PRIVATE-TOKEN":"ic7tQSERwW4SnsdxXfuD"})
	numPages = int(req.headers.get('X-Total-Pages'))
	count = 0
	for i in range(1, numPages+1):
		url = "https://gitlab.com/api/v4/projects/14523582/repository/commits?page=" + str(i)
		for i in req.json():
			print(i['committer_name'])
			if (user in i['committer_name']):
				count += 1
	return str(count)

@gitlabBP.route('/issues', methods = ['GET'])
def getAllIssues():
	req = get("https://gitlab.com/api/v4/projects/14523582/issues", headers = {"PRIVATE-TOKEN":"ic7tQSERwW4SnsdxXfuD"})
	return str(req.headers.get('X-Total'))

@gitlabBP.route('/issues/<user>', methods = ['GET'])
def getUserIssues(user):
	req = get("https://gitlab.com/api/v4/projects/14523582/issues", headers = {"PRIVATE-TOKEN":"ic7tQSERwW4SnsdxXfuD"})
	count = 0
	for i in req.json():
		if (user in i['committer_name']):
			count += 1
	return str(count)

@gitlabBP.route('/test', methods = ['GET'])
def hello_world():
    return 'Hello world'
