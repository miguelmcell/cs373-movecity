from flask import Flask
import flask_restless
from sqlalchemy import Column, Integer, Unicode
from sqlalchemy import ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from flask_cors import CORS

from googlemaps import places
import googlemaps

gmaps = googlemaps.Client(key='AIzaSyBg4GQQdNSceQbeOmVi5hpbYlAi7FIQvJc')


# mysql+mysqldb://root@/<dbname>?unix_socket=/cloudsql/<projectid>:<instancename>
# mysql+mysqldb://root:hackerrank_sucks@127.0.0.1:3306/[DATABASE_NAME]
# SQLAlchemy setup
app = Flask(__name__)
cors = CORS(app)
engine = create_engine(
    "mysql+mysqldb://root:hackerrank_sucks@35.239.108.58:3306/MoveCity?charset=utf8", convert_unicode=True, pool_size=30, max_overflow=10
)
Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
mysession = scoped_session(Session)
Base = declarative_base()
Base.metadata.bind = engine

# RESTLess setup
manager = flask_restless.APIManager(app, session=mysession)


@app.route('/getCityImage/<address>')
def getCityImage(address):
    url = 'https://maps.googleapis.com/maps/api/place/textsearch/json'
    test = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=500&photoreference=&key=AIzaSyBg4GQQdNSceQbeOmVi5hpbYlAi7FIQvJc'
    output = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=500&photoreference='
    # geocode_result = gmaps.geocode('Austin, TX')
    candidate_id = places.find_place(gmaps,address, 'textquery')['candidates'][0]['place_id']
    photo_ref = places.place(gmaps,candidate_id,fields=['photo'])['result']['photos'][0]['photo_reference']
    output += photo_ref + '&key=AIzaSyBg4GQQdNSceQbeOmVi5hpbYlAi7FIQvJc'
    return output

@app.route('/status')
def getStatus():
    return "active"

# DOMs
class City(Base):
    __tablename__ = "City"
    idCity = Column(Integer, primary_key=True)
    CityName = Column(Unicode)
    State = Column(Unicode)


class Quality(Base):
    __tablename__ = "QOL"
    QOL_idCity = Column(Integer, ForeignKey("City.idCity"), primary_key=True)
    Housing = Column(Integer)
    Cost_of_Living = Column(Integer)
    Commute = Column(Integer)
    Healthcare = Column(Integer)
    Education = Column(Integer)
    Environmental_Quality = Column(Integer)
    Tax = Column(Integer)
    State = Column(Unicode)
    CityName = Column(Unicode)


class Climate(Base):
    __tablename__ = "Climate"
    Climate_idCity = Column(Integer, ForeignKey("City.idCity"), primary_key=True)
    Precipitation = Column(Integer)
    RainDays = Column(Integer)
    Avg_Temp = Column(Integer)
    Annual_High = Column(Integer)
    Annual_Low = Column(Integer)
    State = Column(Unicode)
    CityName = Column(Unicode)


class Crime(Base):
    __tablename__ = "Crime"
    Crime_idCity = Column(Integer, ForeignKey("City.idCity"), primary_key=True)
    Aggravated_Assault = Column(Integer)
    Arson = Column(Integer)
    Burglary = Column(Integer)
    Homicide = Column(Integer)
    Larceny = Column(Integer)
    Property_Crime = Column(Integer)
    Vehicle_Theft = Column(Integer)
    Robbery = Column(Integer)
    Violent_Crime = Column(Integer)
    State = Column(Unicode)
    CityName = Column(Unicode)


Base.metadata.create_all()


# Creating API endpoints
manager.create_api(City, methods=["GET", "POST"], results_per_page=0)
manager.create_api(Quality, methods=["GET", "POST"], results_per_page=0)
manager.create_api(Climate, methods=["GET", "POST"], results_per_page=0)
manager.create_api(Crime, methods=["GET", "POST"], results_per_page=0)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, threaded=True)
